#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   score_test.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   11 Aug 2021

@brief  tests for the Djudgement classes

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import unittest
import sqlite3

from . import mocks

from robodjudge.data import (NumericGradeFactory,
                             BooleanGradeFactory,
                             TextGradeFactory,
                             DjudgementFactory,
                             GradeError, Message,
                             Djudge, User,
                             Post, Participant, Thread)

from datetime import datetime
from .user_test import setup_users_helper


class DjudgementCheck(unittest.TestCase):
    def setUp(self):
        self.connection = sqlite3.connect(":memory:")
        setup_users_helper(self.connection)
        Post.create_table(self.connection)
        Message.create_table(self.connection)
        self.connection.commit()
        self.dj_djudge = Djudge.insert(self.connection, mocks.author1)
        self.post = mocks.sotd_stream().__next__()
        self.Djudgement = DjudgementFactory(
            {"dq": BooleanGradeFactory(default_=False).cls,
             'entertainment': NumericGradeFactory(0, 4.).cls,
             "comment": TextGradeFactory().cls}).cls
        self.Djudgement.create_table(self.connection)
        Thread.create_table(self.connection)

    def test_constructor(self):
        djudgement = self.Djudgement(self.post, self.dj_djudge, 12,
                                     entertainment=1.2,
                                     comment="MMOC")
        with self.assertRaises(GradeError):
            self.Djudgement(self.post, self.dj_djudge, 12,
                            dq=False, entertainment=4.5,
                            comment="MMOC")

    def test_insert(self):
        dj_post = Post.insert(self.connection, self.post)
        djudgement = self.Djudgement.insert(self.connection,
                                            dj_post, self.dj_djudge,
                                            entertainment=1.2,
                                            comment="MMOC")
        self.assertEqual(djudgement.pk, 1)
        djudgement_recovered = self.Djudgement.select(self.connection,
                                                      djudgement.pk)
        self.assertEqual(djudgement.djudge.pk, djudgement_recovered.djudge.pk)
        self.assertEqual(djudgement.post.pk, djudgement_recovered.post.pk)

    def test_get_for_participants(self):
        self.test_insert()
        participant = Participant.fetch(self.connection, self.post.author)
        djudgements = self.Djudgement.get_for_participant(self.connection,
                                                          participant)

    def test_update(self):
        dj_post = Post.insert(self.connection, self.post)
        djudgement = self.Djudgement.insert(self.connection,
                                            dj_post, self.dj_djudge,
                                            entertainment=1.2,
                                            comment="MMOC")
        self.assertEqual(djudgement.pk, 1)
        body = """
Entertainment::2.8;;
DQ:: yes;;
Comment:: I've got a lot to say

Joey _Ramone_"""
        subject = "not important"
        sender = "Alice"
        recipient = "Bob"
        msg = Message.insert_manual(self.connection, 1, 2, subject, body,
                                    datetime.now().timestamp(), "aeuathus")
        djudgement.update(self.connection, msg)
        self.assertEqual(djudgement.grades["entertainment"], 2.8)

    def test_fetch(self):
        dj_post = Post.insert(self.connection, self.post)
        djudgement = self.Djudgement.insert(self.connection,
                                            dj_post, self.dj_djudge,
                                            entertainment=1.2,
                                            comment="MMOC")
        self.assertEqual(djudgement.pk, 1)
        djudgement_recovered = self.Djudgement.fetch(self.connection,
                                                     djudgement.post,
                                                     djudgement.djudge)
        self.assertEqual(djudgement.djudge.pk, djudgement_recovered.djudge.pk)
        self.assertEqual(djudgement.post.pk, djudgement_recovered.post.pk)

    def test_reject_double_jeopardy(self):
        dj_post = Post.insert(self.connection, self.post)
        self.Djudgement.insert(self.connection,
                               dj_post, self.dj_djudge,
                               entertainment=1.2,
                               comment="MMOC")

        with self.assertRaises(sqlite3.IntegrityError):
            self.Djudgement.insert(self.connection,
                                   dj_post, self.dj_djudge,
                                   entertainment=1.2,
                                   comment="MMOC")

    def test_parse_djudgement(self):
        body = """
Entertainment::3.4;;
DQ:: yes;;
Comment:: I've got a lot to say

Joey _Ramone_"""
        subject = "not important"
        sender = "Alice"
        recipient = "Bob"
        grades = self.Djudgement.parse_djudgement(
            Message.insert_manual(self.connection, 1, 2, subject, body,
                                  datetime.now().timestamp(), "aeuathus"))

        self.assertEqual(grades["entertainment"], 3.4)
        self.assertEqual(grades["dq"], True)
        self.assertEqual(
            grades["comment"],
            "I've got a lot to say\n\nJoey _Ramone_")

    def test_instructions(self):
        reference_text = """`dq :: <'yes' or 'no' (default is 'False')> ;;`  
`entertainment :: <Value ∈ [0, 4.0] (bounds included)> ;;`  
`comment :: <free text without the tokens '::' or ';;'> ;;`  

This could for instance look like this:

`dq :: no ;;`  
`entertainment :: 2.8 ;;`  
`comment :: Vtephen_jk and Witchy_blooper ;;`  
Categories with a default value can be omitted."""
        instruction_text = self.Djudgement.instructions()
        print(instruction_text)

        self.assertEqual(instruction_text, reference_text)

    def test_representation(self):
        dj_post = Post.insert(self.connection, self.post)
        djudgement = self.Djudgement.insert(self.connection,
                                            dj_post, self.dj_djudge,
                                            entertainment=1.2,
                                            comment="MMOC")
        djudgement_str = """Djudge u/automod gave the following scores:

* dq: False (default: False)
* entertainment: 1.2 ∈ [0, 4.0]
* comment: '''MMOC'''

for [this post](r/Author_A) by u/Author_A in thread [Saturday SOTD Thread - Aug 07, 2021](r/link2)."""
        self.assertEqual(djudgement.__repr__(), djudgement_str)
