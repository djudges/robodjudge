#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   test_helpers.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   26 Aug 2021

@brief  tests for helper functions

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import unittest

from robodjudge import list_table


class ListTableCheck(unittest.TestCase):
    def setUp(self):
        self.h3 = ("GEMtleperson", "#submitted", "#disqualified")
        self.l3 = [("Alice", '5/5', 0),
                   ("Bob", '5/5', 1),
                   ("Charlie", '4/5', 2)]

    def test_three(self):
        table = list_table(self.l3, self.h3)
        reference = """\n
| GEMtleperson | #submitted | #disqualified |
|:-------------|-----------:|--------------:|
| Alice        |        5/5 |             0 |
| Bob          |        5/5 |             1 |
| Charlie      |        4/5 |             2 |"""

        self.assertEqual(table, reference[2:])
