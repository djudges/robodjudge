#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   test_thread.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   30 Jul 2021

@brief  tests thread representations

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import unittest
import sqlite3

from robodjudge.data import Thread


def setup_threads_helper(connection):
    Thread.create_table(connection)
    Thread.insert_manual(
        connection,
        "Monday Fancy February SOTD Thread", "eaueo", "r/link/", 12456)


class ThreadCheck(unittest.TestCase):
    def setUp(self):
        self.connection = sqlite3.connect(":memory:")
        setup_threads_helper(self.connection)
        self.connection.commit()

    def test_insert(self):
        thread = Thread.insert_manual(self.connection,
                                      "Tuesday Fancy February SOTD Thread",
                                      "aeua", "r/perma/link", 123456)
        self.assertEqual(thread.id, "aeua")

    def test_select(self):
        thread = Thread.select(self.connection, 1)
        self.assertEqual(thread.id, "eaueo")
        self.assertRaises(TypeError, Thread.select, self.connection, 2)

    def test_harvest(self):
        thread = Thread.select(self.connection, 1)
        self.assertFalse(thread.harvested)
        self.assertEqual(len(list(Thread.get_all(self.connection, False))), 1)
        self.assertEqual(len(list(Thread.get_all(self.connection, True))), 0)
        thread.set_harvested(self.connection)
        self.assertTrue(thread.harvested)

        thread_second_instance = Thread.select(self.connection, 1)
        self.assertTrue(thread_second_instance.harvested)
        self.assertEqual(len(list(Thread.get_all(self.connection, True))), 1)
        self.assertEqual(len(list(Thread.get_all(self.connection, False))), 0)
