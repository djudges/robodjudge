#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   mocks.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   09 Aug 2021

@brief  simple praw mocks for functional tests

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from unittest.mock import (Mock, patch)
from collections import namedtuple

Redditor = namedtuple("Redditor", ["name", 'id'])
Submission = namedtuple("Submission", ["author", "title", "created_utc", "id",
                                       "permalink", "comments"])


class DetachedUser():
    def __init__(self, name, id):
        self.name = name
        self.id = id


class DetachedMessage():
    def __init__(self, author, dest, subject, body, created_utc, id):
        self.author = author
        self.dest = dest
        self.subject = subject
        self.body = body
        self.created_utc = created_utc
        self.id = id

    def reply(self, *args, **kwargs):
        pass


author1 = Redditor("automod", 1)

author_a = Redditor("Author_A", 2)
author_b = Redditor("Author_B", 3)
author_c = Redditor("Author_C", 4)

MockMsg = namedtuple("Message",
                     ["author", 'dest', "subject", "body", "created_utc"])




def submissionm1():
    return Submission(author1, "Monday SOTD Thread - Aug 09, 2021",
                      1628617472.010036, "TOKENM0", "r/linkm1", [])
def submission0():
    return Submission(author1, "Monday SOTD Thread - Aug 09, 2021",
                      1628518028.665743, "TOKEN0", "r/link0", [])
def submission1():
    return Submission(author1, "Sunday SOTD Thread - Aug 08, 2021",
                      1628431709.582084, "TOKEN1", "r/link1", [])
def submission1b():
    return Submission(author1,
               "Sunday Daily Questions Thread - Aug 08, 2021",
                      1628431709.582084, "TOKEN1b", "r/link1b", [])
def submission2():
    return Submission(author1, "Saturday SOTD Thread - Aug 07, 2021",
                      1628345337.337588, "TOKEN2", "r/link2", [])

def mock_submission(submission):
    submission_mock = Mock()
    submission_mock.author = submission.author
    submission_mock.title = submission.title
    submission_mock.created_utc = submission.created_utc
    submission_mock.id = submission.id
    submission_mock.comments = submission.comments
    return submission_mock

def filtered_submissions():
    def gen():
        for sub in (submission2(), submission1(), submission0()):
            comments = list()
            # make a valid comment
            comment = Mock()
            comment.submission = sub
            comment.author = author_a
            comment.body = "keyword" + sub.title
            comment.created_utc = sub.created_utc + 5
            comment.id = sub.id + comment.author.name
            comment.permalink = "r/" + comment.author.name
            sub.comments.append(comment)
            # make an irrelevant comment
            comment = Mock()
            comment.submission = sub
            comment.author = author_b
            comment.body = "random stuff" + sub.title
            comment.created_utc = sub.created_utc + 10
            comment.id = sub.id + comment.author.name
            comment.permalink = "r/" + comment.author.name
            sub.comments.append(comment)
            # make a late comment
            comment = Mock()
            comment.submission = sub
            comment.author = author_c
            comment.body = "keyword" + sub.title
            comment.created_utc = sub.created_utc + 100000
            comment.id = sub.id + comment.author.name
            comment.permalink = "r/" + comment.author.name
            sub.comments.append(comment)
            yield sub

    return [mock_submission(sub) for sub in gen()]


def sotd_stream():
    for sub in filtered_submissions():
        yield sub.comments[0]
