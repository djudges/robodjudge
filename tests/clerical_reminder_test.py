#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   clerical_reminder_test.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   14 Aug 2021

@brief  test the ClericalReminder delay queue class

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import multiprocessing as mp
from datetime import datetime, timedelta
import time
import sqlite3

import unittest

from robodjudge.logistics import ClericalReminder
from robodjudge.data import (DjudgeInvitation, Djudge, Participant, User,
                             Thread, Post, Reminder)
from .mocks import author1, sotd_stream
from datetime import datetime


class NewClericalReminderCheck(unittest.TestCase):
    def setUp(self):
        reminder_delay = timedelta(seconds=.02)
        self.connection = sqlite3.connect(":memory:")
        User.create_table(self.connection)
        Djudge.create_table(self.connection)
        Participant.create_table(self.connection)
        self.connection.commit()
        self.djudge = Djudge.insert(self.connection, author1)
        Thread.create_table(self.connection)
        Post.create_table(self.connection)
        self.posts = [Post.insert(self.connection, sotd)
                      for sotd in sotd_stream()]
        DjudgeInvitation.create_table(self.connection)
        self.invite = DjudgeInvitation.insert(self.connection, self.posts[0],
                                              self.djudge)
        Reminder.create_table(self.connection)

        self.reminder = ClericalReminder(self.connection, reminder_delay)

    def test_delay(self):
        reference = self.invite.safety_token
        self.reminder.put(reference)
        available_reminders = list(self.reminder.get())
        self.assertEqual(len(available_reminders), 0)
        self.assertFalse(self.reminder.empty())
        time.sleep(self.reminder.default_delay.total_seconds())
        available_reminders = list(self.reminder.get())
        self.assertEqual(len(available_reminders), 1)
        self.assertEqual(available_reminders[0], reference)
        self.assertTrue(self.reminder.empty())
