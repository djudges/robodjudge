#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   test_users.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   29 Jul 2021

@brief  tests user representation

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import unittest
from unittest.mock import MagicMock
import sqlite3

from robodjudge.data import Participant, Djudge, User, UserError

from . import mocks


def setup_users_helper(connection):
    User.create_table(connection)
    Djudge.create_table(connection)
    Participant.create_table(connection)
    connection.commit()
    User.insert_manual(connection, "CerebralRead", ["Djudge"])
    User.insert_manual(connection, "nhoJ", ["Djudge"])
    User.insert_manual(connection, "LaDrormRa", ["Djudge"])
    User.insert_manual(connection, "CorporalCrackedCorn", ["Participant"])
    User.insert_manual(connection, "CaptainCutCarrots", ["Participant"])
    User.insert_manual(connection, "LieutenantLardedLobster", ["Participant"])
    User.insert_manual(connection, "PrivatePoachedPickle", ["Participant"])
    User.insert_manual(connection, "GeneralGratedGruyere", ["Participant"])


class UsersCheck(unittest.TestCase):
    def setUp(self):
        self.connection = sqlite3.connect(":memory:")
        setup_users_helper(self.connection)
        self.connection.commit()

    def test_unique_roles(self):
        with self.assertRaises(sqlite3.IntegrityError):
            Participant.create_table(self.connection)

        praw_user = MagicMock()
        praw_user.name = "CerebralRead"
        user = Djudge.fetch(self.connection, praw_user)
        with self.assertRaises(sqlite3.IntegrityError):
            Djudge.add_role(self.connection, user)

    def test_all(self):
        users = User.all(self.connection)
        self.assertEqual({user.username for user in users},
                         {"CerebralRead",
                          "nhoJ",
                          "LaDrormRa",
                          "CorporalCrackedCorn",
                          "CaptainCutCarrots",
                          "GeneralGratedGruyere",
                          "LieutenantLardedLobster",
                          "PrivatePoachedPickle"})
        djudges = Djudge.all(self.connection)
        self.assertEqual({user.username for user in djudges},
                         {"CerebralRead",
                          "nhoJ",
                          "LaDrormRa"})
        praw_user = MagicMock()
        praw_user.name = "CerebralRead"
        Participant.add_role(self.connection,
                             Djudge.fetch(self.connection, praw_user))
        participants = Participant.all(self.connection)
        self.assertEqual({participant.username for participant in participants},
                         {"CerebralRead",
                          "CorporalCrackedCorn",
                          "CaptainCutCarrots",
                          "GeneralGratedGruyere",
                          "LieutenantLardedLobster",
                          "PrivatePoachedPickle"})

    def test_exist(self):
        praw_user = MagicMock()
        praw_user.name = "CerebralRead"
        self.assertTrue(User.exists(self.connection, praw_user))
        self.assertTrue(Djudge.exists(self.connection, praw_user))
        self.assertTrue(not Participant.exists(self.connection, praw_user))

        user = Djudge.fetch(self.connection, praw_user)
        Participant.add_role(self.connection, user)

        self.assertTrue(Participant.exists(self.connection, praw_user))

    def test_insert_manual(self):
        participant = User.insert_manual(
            self.connection, "Djudgilla", Participant.role)
        self.assertEqual(participant.pk, 9)

        with self.assertRaises(UserError):
            _ = User.insert_manual(self.connection, "test_name",
                                   ["commentator"])

    def test_retrieve(self):
        with self.assertRaises(UserError):
            _ = Participant.select(self.connection, 3)
        djudge = Djudge.select(self.connection, 3)
        self.assertEqual(djudge.username, "LaDrormRa")

    def test_insert(self):
        djudge = Djudge.insert(self.connection, mocks.author1)
        self.assertEqual(djudge.username, mocks.author1.name)
        self.assertTrue(Djudge.role in djudge.roles)

        # addid the same user, but in different role
        djudge_and_participant = Participant.insert(
            self.connection, mocks.author1)
        self.assertEqual(djudge.username, mocks.author1.name)
        self.assertTrue(Djudge.role in djudge_and_participant.roles)
        self.assertTrue(Participant.role in djudge_and_participant.roles)

        participant = Participant.insert(self.connection, mocks.author_a)
        self.assertEqual(participant.username, mocks.author_a.name)
        self.assertTrue(Participant.role in participant.roles)

    def test_multi_role(self):
        djudge = Djudge.insert(self.connection, mocks.author1)
        Participant.add_role(self.connection, djudge)
        self.assertTrue(Djudge.role in djudge.roles)
        self.assertTrue(Participant.role in djudge.roles)

if __name__ == "__main__":
    unittest.main()
