#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   test_comment_harvester.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   09 Aug 2021

@brief  test for the CommentHarvester class

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import unittest
from unittest.mock import (Mock, patch)
from multiprocessing import JoinableQueue

from .mocks import (Redditor, Submission,
                    filtered_submissions)

from robodjudge.logistics import (DeprecatedCommentHarvester,
                                  stop_iteration_tag)


class CommentHarvesterCheck(unittest.TestCase):
    @patch("robodjudge.logistics.comment_harvester.praw")
    def setUp(self, praw):
        self.submissions = list(filtered_submissions())

        self.bot_conf = {"doesnt": "matter"}
        self.submission_id_queue = JoinableQueue()
        for submission in self.submissions:
            self.submission_id_queue.put(submission.id)
        self.submission_id_queue.put(stop_iteration_tag)
        self.post_id_queue = JoinableQueue()
        self.messaging_queue = JoinableQueue()
        self.posts_filter = lambda posts: (
            post for post
            in posts if (
                "keyword" in post.body and
                post.created_utc - post.submission.created_utc < 90000))
        self.harvester = DeprecatedCommentHarvester(self.bot_conf,
                                          self.submission_id_queue,
                                          self.post_id_queue,
                                          self.messaging_queue,
                                          self.posts_filter)

    @patch("robodjudge.logistics.comment_harvester.praw")
    def test_run(self, praw):
        praw.Reddit.return_value = Mock()
        submission_dict = {sub.id: sub for sub in self.submissions}

        def submission(thread_id):
            return submission_dict[thread_id]

        self.harvester.reddit = Mock()
        self.harvester.reddit.submission.side_effect = submission
        self.harvester.run()
        expected_queue_state = [('post_id', 'TOKEN2Author_A'),
                                ('post_id', 'TOKEN1Author_A'),
                                ('post_id', 'TOKEN0Author_A')]
        queue_state = list()
        for post_id in iter(self.harvester.post_id_queue.get,
                            stop_iteration_tag):
            queue_state.append(post_id)
            self.harvester.post_id_queue.task_done()
        self.harvester.post_id_queue.task_done()

        self.assertEqual(expected_queue_state, queue_state)
