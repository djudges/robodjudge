#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   djudge_invitation_test.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   13 Aug 2021

@brief  tests for the invitation class

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import unittest
from .mocks import author1, sotd_stream
import sqlite3

from robodjudge.data import DjudgeInvitation, Djudge, Participant, User, Thread, Post


class DjudgeInvitationCheck(unittest.TestCase):
    def setUp(self):
        self.connection = sqlite3.connect(":memory:")
        User.create_table(self.connection)
        Djudge.create_table(self.connection)
        Participant.create_table(self.connection)
        self.connection.commit()
        self.djudge = Djudge.insert(self.connection, author1)
        Thread.create_table(self.connection)
        Post.create_table(self.connection)
        self.posts = [Post.insert(self.connection, sotd)
                      for sotd in sotd_stream()]
        DjudgeInvitation.create_table(self.connection)

    def test_construction(self):
        invite = DjudgeInvitation.insert(self.connection, self.posts[0],
                                         self.djudge)

        invite_reconstructed = DjudgeInvitation.select(self.connection,
                                                       invite.pk)

        self.assertEqual(invite_reconstructed.post.pk, invite.post.pk)
        self.assertEqual(invite_reconstructed.djudge.pk, invite.djudge.pk)
        self.assertEqual(invite_reconstructed.safety_token, invite.safety_token)
        self.assertEqual(invite_reconstructed.status, invite.status)

    def test_double_joepardy(self):
        DjudgeInvitation.insert(self.connection, self.posts[0],
                                self.djudge)
        with self.assertRaises(sqlite3.IntegrityError):
            DjudgeInvitation.insert(self.connection, self.posts[0],
                                    self.djudge)

    def test_status_change(self):
        invite = DjudgeInvitation.insert(self.connection, self.posts[0],
                                         self.djudge)
        invite.set_reminded(self.connection)
        invite_reconstructed = DjudgeInvitation.select(self.connection,
                                                       invite.pk)

        self.assertEqual("reminded", invite_reconstructed.status)
        self.assertEqual(invite.status, invite_reconstructed.status)

        invite.set_djudged(self.connection)
        invite_reconstructed = DjudgeInvitation.select(self.connection,
                                                       invite.pk)

        self.assertEqual("djudged", invite_reconstructed.status)
        self.assertEqual(invite.status, invite_reconstructed.status)

        invite.set_delinquent(self.connection)
        invite_reconstructed = DjudgeInvitation.select(self.connection,
                                                       invite.pk)

        self.assertEqual("delinquent", invite_reconstructed.status)
        self.assertEqual(invite.status, invite_reconstructed.status)

    def test_invites_for_post(self):
        invite = DjudgeInvitation.insert(self.connection, self.posts[0],
                                         self.djudge)
        invites = DjudgeInvitation.invites_for_post(self.connection,
                                                    self.posts[0])

        self.assertEqual(invite.pk, invites[0].pk)

        invites = DjudgeInvitation.invites_for_post(self.connection,
                                                    self.posts[1])

        self.assertEqual(0, len(invites))

    def test_fetch(self):
        invite = DjudgeInvitation.insert(self.connection, self.posts[0],
                                         self.djudge)
        fetched = DjudgeInvitation.fetch(self.connection, invite.safety_token)

        self.assertEqual(invite.pk, fetched.pk)
