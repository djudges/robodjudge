#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   test_submission_finder.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   08 Aug 2021

@brief  test the submission fetcher against a mock of praw

Copyright © 2021 Djundjila

Robodjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Robodjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import unittest
from unittest.mock import (Mock, patch)
import re
from multiprocessing import JoinableQueue
from datetime import datetime


from collections import namedtuple

from robodjudge.logistics import SubmissionFinder
from robodjudge.logistics import stop_iteration_tag
praw = Mock()

Redditor = namedtuple("Redditor", ["name"])
Submission = namedtuple("Submission", ["author", "title", "created_utc", "id"])

author1 = Redditor("automod")
submissionm1 = Submission(author1, "Monday SOTD Thread - Aug 09, 2021",
                          1628617472.010036, "TOKENM0")
submission0 = Submission(author1, "Monday SOTD Thread - Aug 09, 2021",
                         1628518028.665743, "TOKEN0")
submission1 = Submission(author1, "Sunday SOTD Thread - Aug 08, 2021",
                         1628431709.582084, "TOKEN1")
submission1b = Submission(author1,
                          "Sunday Daily Questions Thread - Aug 08, 2021",
                          1628431709.582084, "TOKEN1b")
submission2 = Submission(author1, "Saturday SOTD Thread - Aug 07, 2021",
                         1628345337.337588, "TOKEN2")

class SubmissionFinderCheck(unittest.TestCase):
    def setUp(self):
        self.bot_conf = "doesn't matter"
        self.subreddit = "shetwaving"
        self.patterns = [re.compile(".* SOTD.*")]
        self.submission_id_queue = JoinableQueue()
        self.nb_submissions = 3
        self.since = datetime.today()

    @patch("robodjudge.logistics.submission_finder.praw")
    def test_constructor(self, praw):
        praw.Reddit().subreddit().new.return_value = [submission1, submission1b,
                                                      submission2]

        praw.Reddit().subreddit().stream().submissions.return_value = [
            submission0, submissionm1]
        sub_finder = SubmissionFinder(self.bot_conf, self.subreddit,
                                      self.patterns, self.submission_id_queue,
                                      self.nb_submissions, self.since)
        sub_finder.run()

        def close_out_queue():
            for item in iter(self.submission_id_queue.get, stop_iteration_tag):
                yield item
                self.submission_id_queue.task_done()
            self.submission_id_queue.task_done()
        self.assertEqual(
            list(close_out_queue()),
            ["TOKEN2", "TOKEN1", "TOKEN0"])
        self.submission_id_queue.join()

if __name__ == "__main__":
    unittest.main()
