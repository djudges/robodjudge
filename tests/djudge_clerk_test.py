#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   djudge_clerk_test.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   11 Aug 2021

@brief  tests for the DjudgeClerk class, the main logistics class of RoboDjudge

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import multiprocessing as mp
import threading
import queue
from .mocks import sotd_stream
import unittest
from unittest.mock import MagicMock, patch
import sqlite3

from datetime import timedelta, datetime
from collections import namedtuple

from robodjudge import data
from robodjudge import logistics
from . import mocks


class DjudgeClerkCheck(unittest.TestCase):
    def setUp(self):
        self.sotds = list(sotd_stream())
        self.reddit = MagicMock()
        self.connection = sqlite3.connect(":memory:")
        self.mum = mocks.Redditor("MaternalDjudge", 5)

        self.Djudgement = data.DjudgementFactory(
            {"dq": data.BooleanGradeFactory(default_=False).cls,
             'entertainment': data.NumericGradeFactory(0, 4.).cls,
             "comment": data.TextGradeFactory().cls}).cls
        data.User.create_table(self.connection)
        data.Djudge.create_table(self.connection)
        data.Participant.create_table(self.connection)
        self.connection.commit()
        data.Djudge.insert(self.connection, mocks.author1)
        data.Thread.create_table(self.connection)
        data.Post.create_table(self.connection)
        data.DjudgeInvitation.create_table(self.connection)
        data.Reminder.create_table(self.connection)
        data.Message.create_table(self.connection)
        data.Acknowledgement.create_table(self.connection)
        self.Djudgement.create_table(self.connection)

        self.sotds = list(mocks.sotd_stream())
        logistics.djudge_clerk.praw = MagicMock()

        class dumbfetcher():
            def fetch():
                return []
            
            def fetchall():
                return []
            
        self.clerk = logistics.DjudgeClerk(
            self.reddit,
            self.connection,
            self.mum,
            self.Djudgement,
            dumbfetcher(),
            "GEMsOfWisdom",
            reminder_delay=timedelta(seconds=.002),
            message_rate=timedelta(seconds=.001))
        sotd_dict = {sotd.id: sotd for sotd in self.sotds}

        def sotd(thread_id):
            return sotd_dict[thread_id]

        self.clerk.reddit = MagicMock()
        self.clerk.reddit.comment.side_effect = sotd

    def test_construction(self):
        pass

    def test_new_post_without_invitation(self):
        self.clerk.post_fetcher = MagicMock()
        self.clerk.post_fetcher.fetch.return_value = [
            mocks.filtered_submissions()[0].comments[0]]
        self.clerk.post_fetcher.fetch_all.return_value = [
            mocks.filtered_submissions()[0].comments[0]]

        self.clerk.have_open_invitations = MagicMock()
        self.clerk.have_open_invitations.return_value = False

        self.clerk.run()

    def test_new_post(self):
        self.clerk.post_fetcher = MagicMock()
        self.clerk.post_fetcher.fetch.return_value = [
            mocks.filtered_submissions()[0].comments[0]]
        self.clerk.post_fetcher.fetch_all.return_value = [
            mocks.filtered_submissions()[0].comments[0]]

        self.clerk.have_open_invitations = MagicMock()
        self.clerk.have_open_invitations.return_value = False
        self.clerk.reminder_clerk.empty = MagicMock()
        self.clerk.reminder_clerk.empty.return_value = True

        self.clerk.run()
        self.assertEqual(
            data.DjudgeInvitation.count_status(self.connection, "invited"),
            1)

    def test_double_post(self):
        self.clerk.post_fetcher = MagicMock()
        self.clerk.post_fetcher.fetch.return_value = [
            mocks.filtered_submissions()[0].comments[0],
            mocks.filtered_submissions()[0].comments[0]]
        self.clerk.post_fetcher.fetch_all.return_value = [
            mocks.filtered_submissions()[0].comments[0],
            mocks.filtered_submissions()[0].comments[0]]

        self.clerk.have_open_invitations = MagicMock()
        self.clerk.have_open_invitations.return_value = False
        self.clerk.reminder_clerk.empty = MagicMock()
        self.clerk.reminder_clerk.empty.return_value = True

        self.clerk.run()

        self.assertEqual(
            data.DjudgeInvitation.count_status(self.connection, "invited"),
            1)

    def test_new_post_with_invitatons(self):
        self.clerk.post_fetcher = MagicMock()
        self.clerk.post_fetcher.fetch.return_value = [
            mocks.filtered_submissions()[0].comments[0]]
        self.clerk.post_fetcher.fetch_all.return_value = [
            mocks.filtered_submissions()[0].comments[0]]

        self.clerk.run()
        self.assertEqual(
            data.DjudgeInvitation.count_status(self.connection, "delinquent"),
            1)

    @patch("robodjudge.data.djudge_invitation.secrets")
    def test_new_post_with_invitatons_and_djudgement(self, secrets):
        secrets.token_urlsafe.return_value = "ABCDEFG"

        self.clerk.post_fetcher = MagicMock()
        self.clerk.post_fetcher.fetch.return_value = [
            mocks.filtered_submissions()[0].comments[0]]
        self.clerk.post_fetcher.fetch_all.return_value = [
            mocks.filtered_submissions()[0].comments[0]]

        subject = (
            "Re: Re: Inviting Djudge u/automod to djudge post id ABCDEFG")
        body = """ entertainment::1.8;;comment::smart musings"""
        created_utc = datetime.now().timestamp()
        id = "ea367e4b"

        # self.job_queue.put(("djudgement", data.Message.insert(
        #     self.connection,
        #     mocks.DetachedMessage(
        #         mocks.DetachedUser(mocks.author1.name),
        #         mocks.DetachedUser(mocks.author_b.name),
        #         subject,
        #         body,
        #         created_utc,
        #         id))))

        self.clerk.run()

        self.assertEqual(
            data.DjudgeInvitation.count_status(self.connection, "delinquent"),
            1)

    @patch("robodjudge.data.djudge_invitation.secrets")
    def test_new_post_with_invitatons_and_djudge_error(self, secrets):
        secrets.token_urlsafe.return_value = "HIJKLMN"
        self.clerk.post_fetcher = MagicMock()
        self.clerk.post_fetcher.fetch.return_value = [
            mocks.filtered_submissions()[0].comments[0]]
        self.clerk.post_fetcher.fetch_all.return_value = [
            mocks.filtered_submissions()[0].comments[0]]

        subject = (
            "Re: Re: Inviting Djudge u/automod to djudge post id HIJKLMN")
        body = """ entertainement::1.8;;comment::smart musings"""
        created_utc = datetime.now().timestamp()
        id = "ea367e4b"
        messages = {mocks.DetachedMessage(
            mocks.DetachedUser(mocks.author1.name, mocks.author1.id),
            mocks.DetachedUser(mocks.author_b.name, mocks.author_b.id),
            subject,
            body,
            created_utc,
            id)}
        self.clerk.hook = lambda self: self.handle_new_djudgement(
            messages.pop()) if messages else None

        self.clerk.run()

        self.assertEqual(
            data.DjudgeInvitation.count_status(self.connection, "delinquent"),
            1)

    def test_new_post_with_invitatons_and_djudgement_message(self):
        self.clerk.post_fetcher = MagicMock()
        self.clerk.post_fetcher.fetch.return_value = [
            mocks.filtered_submissions()[0].comments[0]]
        self.clerk.post_fetcher.fetch_all.return_value = [
            mocks.filtered_submissions()[0].comments[0]]

        reply_queue = queue.Queue()
        self.clerk.inbox_handler.__iter__ = MagicMock()
        self.clerk.inbox_handler.__iter__.return_value = iter(
            reply_queue.get, None)

        function_copy = self.clerk.handle_outbox_message
        def mock_djudge(message):
            print(message.subject.lower())
            if "query" not in message.subject.lower():
                subject = f"Re: {message.subject}"
                body = """ entertainment::1.8;;comment::smart musings"""
                reply_queue.put(
                    mocks.DetachedMessage(
                        mocks.DetachedUser(mocks.author1.name,
                                           mocks.author1.id),
                        mocks.DetachedUser(mocks.author_a.name,
                                           mocks.author_a.id),
                        subject, body, datetime.now().timestamp(),
                        "aoeuareucaosetu"))
                reply_queue.put(None)
            else:
                function_copy(message)

        self.clerk.handle_outbox_message = mock_djudge
        self.clerk.run()

        self.assertEqual(
            data.DjudgeInvitation.count_status(self.connection, "djudged"),
            1)

    def test_new_post_with_invitatons_and_djudgement_message_bad_format(self):
        self.clerk.post_fetcher = MagicMock()
        self.clerk.post_fetcher.fetch.return_value = [
            mocks.filtered_submissions()[0].comments[0]]
        self.clerk.post_fetcher.fetch_all.return_value = [
            mocks.filtered_submissions()[0].comments[0]]

        reply_queue = queue.Queue()
        self.clerk.inbox_handler.__iter__ = MagicMock()
        self.clerk.inbox_handler.__iter__.return_value = iter(
            reply_queue.get, None)

        function_copy = self.clerk.handle_outbox_message
        def mock_djudge(message):
            if "query" not in message.subject.lower():
                subject = f"Re: {message.subject}"
                body = """ entertainment::1.8;;comment::smart musings;;"""
                reply_queue.put(
                    mocks.DetachedMessage(
                        mocks.DetachedUser(mocks.author1.name,
                                           mocks.author1.id),
                        mocks.DetachedUser(mocks.author_a.name,
                                           mocks.author_a.id),
                        subject, body, datetime.now().timestamp(),
                        "aoeuareucaosetu"))
                reply_queue.put(None)
            else:
                function_copy(message)

        self.clerk.handle_outbox_message = mock_djudge
        self.clerk.run()

        self.assertEqual(
            data.DjudgeInvitation.count_status(self.connection, "djudged"),
            1)
