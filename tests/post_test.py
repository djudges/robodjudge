#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   test_posts.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   30 Jul 2021

@brief  tests for posts representation

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import unittest
import sqlite3
from . import user_test
from . import thread_test

from robodjudge.data import Post


def setup_posts_helper(connection):
    user_test.setup_users_helper(connection)
    thread_test.setup_threads_helper(connection)
    Post.create_table(connection)
    author_id = 4
    thread_id = 1
    Post.insert_manual(connection, "oeafe", "an interesting\nparagraph",
                       author_id, thread_id, "https://google.com", 14852431)


class PostsCheck(unittest.TestCase):
    def setUp(self):
        self.connection = sqlite3.connect(":memory:")
        setup_posts_helper(self.connection)
        self.connection.commit()

    def test_insert(self):
        post = Post.insert_manual(self.connection, "oeafea",
                                  "an interesting\nparagraph",
                                  4, 1, "https://google.com", 51267)
        self.assertEqual(post.author.username, "CorporalCrackedCorn")

    def test_select(self):
        post = Post.select(self.connection, 1)
        self.assertEqual(post.comment_id, "oeafe")
