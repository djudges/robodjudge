#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   reminder_test.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   24 Aug 2021

@brief  tests for the reminder storage

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import unittest
from .mocks import author1, sotd_stream
from datetime import datetime
import sqlite3

from robodjudge.data import (DjudgeInvitation, Djudge, Participant, User,
                             Thread, Post, Reminder)


class ReminderCheck(unittest.TestCase):
    def setUp(self):
        self.connection = sqlite3.connect(":memory:")
        User.create_table(self.connection)
        Djudge.create_table(self.connection)
        Participant.create_table(self.connection)
        self.connection.commit()
        self.djudge = Djudge.insert(self.connection, author1)
        Thread.create_table(self.connection)
        Post.create_table(self.connection)
        self.posts = [Post.insert(self.connection, sotd)
                      for sotd in sotd_stream()]
        DjudgeInvitation.create_table(self.connection)
        self.invite = DjudgeInvitation.insert(self.connection, self.posts[0],
                                              self.djudge)
        Reminder.create_table(self.connection)

    def test_everything(self):
        self.assertEqual([], Reminder.all(self.connection))
        now = datetime.now()
        Reminder.insert(self.connection, self.invite.safety_token, now)
        reminders = Reminder.all(self.connection)
        self.assertEqual(len(reminders), 1)
        self.assertEqual(reminders[0].safety_token, self.invite.safety_token)

