#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   message_test.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   21 Aug 2021

@brief  test message db definition

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import unittest
from unittest.mock import MagicMock
import sqlite3

from datetime import datetime
import pickle

from .user_test import setup_users_helper

from robodjudge.data import Message, MessageError

from . import mocks


def setup_messages_helper(connection):
    setup_users_helper(connection)
    Message.create_table(connection)
    Message.insert_manual(
        connection=connection,
        author_pk=1,
        dest_pk=2,
        subject="subject",
        body="body",
        created_utc=123456,
        message_id="ae4e65")
    Message.insert_manual(
        connection=connection,
        author_pk=2,
        dest_pk=3,
        subject="subject 2",
        body="body 2",
        created_utc=123456,
        message_id="ae4b65")
    connection.commit()


class MessageCheck(unittest.TestCase):
    def setUp(self):
        self.connection = sqlite3.connect(":memory:")
        setup_messages_helper(self.connection)
        self.praw_message = MagicMock()
        self.praw_message.author = mocks.author_a
        self.praw_message.dest = mocks.author_b
        self.praw_message.subject = "praw subject"
        self.praw_message.body = "praw body"
        self.praw_message.created_utc = datetime.now().timestamp()
        self.praw_message.id = "a4e6e24"

    def test_insert_manual(self):
        msg = Message.insert_manual(
            connection=self.connection,
            author_pk=1,
            dest_pk=2,
            subject="subject",
            body="body",
            created_utc=123456,
            message_id="ae4e45")
        self.assertEqual(msg.author.username, "CerebralRead")
        self.assertEqual(msg.dest.username, "nhoJ")

    def test_self_message(self):
        with self.assertRaises(sqlite3.IntegrityError):
            Message.insert_manual(
                connection=self.connection,
                author_pk=1,
                dest_pk=1,
                subject="subject",
                body="body",
                created_utc=123456,
                message_id="ae4e65")

    def test_double_message_id(self):
        with self.assertRaises(sqlite3.IntegrityError):
            Message.insert_manual(
                connection=self.connection,
                author_pk=1,
                dest_pk=1,
                subject="subject",
                body="body",
                created_utc=123456,
                message_id="ae4e65")

    def test_praw_message(self):
        msg = Message.insert(self.connection, self.praw_message)

        self.assertEqual(msg.author.username, mocks.author_a.name)
        self.assertEqual(msg.dest.username, mocks.author_b.name)

    def test_select(self):
        msg = Message.select(self.connection, 1)
        self.assertEqual(msg.message_id, "ae4e65")

    def test_fetch(self):
        Message.insert(self.connection, self.praw_message)
        msg = Message.fetch(self.connection, self.praw_message)
        return msg

    def test_pickle(self):
        msg = self.test_fetch()
        pickled_msg = pickle.dumps(msg)
        msg_retrieved = pickle.loads(pickled_msg)
        self.assertEqual(msg.__dict__, msg_retrieved.__dict__)
