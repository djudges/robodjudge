#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   main_test_suite.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   30 Jul 2021

@brief  calls all tests

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import sys
sys.path.append("..")
import unittest

from .user_test import UsersCheck
from .post_test import PostsCheck
from .thread_test import ThreadCheck
from .submission_finder_test import SubmissionFinderCheck
from .comment_harvester_test import CommentHarvesterCheck

if __name__ == "__main__":
    unittest.main()
