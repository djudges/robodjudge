#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   acknowledgement_test.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   02 Sep 2021

@brief  tests for the acknowledgement storage

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import unittest

import sqlite3

from robodjudge.data import Post, Acknowledgement

from .post_test import setup_posts_helper


def setup_acknowledgements_helper(connection):
    Acknowledgement.create_table(connection)


class AcknowledgementCheck(unittest.TestCase):
    def setUp(self):
        self.connection = sqlite3.connect(":memory:")
        setup_posts_helper(self.connection)
        setup_acknowledgements_helper(self.connection)
        self.connection.commit()

    def test_insert(self):
        posts = Post.all(self.connection)
        for post in posts:
            ack = Acknowledgement.insert(self.connection, post)
            self.assertEqual(post.pk, ack.post.pk)

    def test_exists(self):
        self.test_insert()
        posts = Post.all(self.connection)
        for post in posts:
            self.assertTrue(Acknowledgement.exists(self.connection, post))

    def test_fetch(self):
        self.test_insert()
        posts = Post.all(self.connection)
        for post in posts:
            ack = Acknowledgement.fetch(self.connection, post)
            self.assertEqual(ack.post.pk, post.pk)

    def test_select(self):
        self.test_insert()
        self.assertTrue(isinstance(Acknowledgement.select(self.connection, 1),
                                   Acknowledgement))

    def test_unacknowledged(self):
        Post.insert_manual(self.connection, "oeafea", "an interesting\nparagraph",
                           5, 1, "https://google.com", 14852431)
        self.assertTrue(len(list(Post.all(self.connection))) > 1)
        unack = list(Acknowledgement.get_unacknowledged_posts(self.connection))
        self.assertEqual(len(unack), 1)
        unack = list(Acknowledgement.get_unacknowledged_posts(self.connection,
                                                              limit=2))
        self.assertEqual(len(unack), 2)

        unack = list(Acknowledgement.get_unacknowledged_posts(self.connection,
                                                              limit=5))
        self.assertEqual(len(unack), 2)
        Acknowledgement.insert(self.connection, unack[0])
        unack = list(Acknowledgement.get_unacknowledged_posts(self.connection,
                                                              limit=5))
        self.assertEqual(len(unack), 1)
        Acknowledgement.insert(self.connection, unack[0])
        unack = list(Acknowledgement.get_unacknowledged_posts(self.connection,
                                                              limit=5))
        self.assertEqual(len(unack), 0)
