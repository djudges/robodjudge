#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   helpers.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   13 Aug 2021

@brief  little helper functions

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import re
import time
from datetime import datetime, timedelta
import praw


def truncate_str(text, max_len):
    return text if len(text) <= max_len else text[:max_len-3] + "..."


def truncate_subject(text):
    max_subject_length = 100
    return truncate_str(text, max_subject_length)


def truncate_body(text):
    max_body_length = 10000
    return truncate_str(text, max_body_length)


def sleep_with_countdown(delay, interval=timedelta(seconds=1.)):
    start = datetime.now()
    deadline = start + timedelta(seconds=delay)
    next_update = start + interval
    while next_update < deadline:
        next_update += interval
        now = datetime.now()
        to_wait = int(round((deadline - now).total_seconds()))
        print(f"\rNext attempt in {to_wait} seconds", end="")
        time.sleep(max(0., (next_update-now).total_seconds()))
    print()


def message_with_rate_limit(user, subject, body):
    _pattern = re.compile(
        r'Take a break for (?P<number>[0-9]+) (?P<unit>\w+)s? '
        r'before trying again\.$',
        re.IGNORECASE)
    try:
        user.message(subject, truncate_body(body))
    except praw.exceptions.RedditAPIException as err:
        time_map = {
            'millisecond': 1e-3,
            'second': 1,
            'minute': 60,
            'hour': 60 * 60,
        }
        matches = re.search(_pattern, err.message)
        print(matches)
        if not matches:
            raise Exception(
                f'Unable to parse rate limit message {err.message!r}')
            return
        delay = (
            int(matches["number"]) * time_map[matches["unit"].rstrip("s")]
            + 1 * time_map[matches["unit"].rstrip("s")])
        print(
            f"ratelimit hit, message was {err}. Sleeping for {delay} seconds.")
        sleep_with_countdown(delay)
        user.message(truncate_subject(subject), truncate_body(body))


def reply_with_rate_limit(message, body):
    _pattern = re.compile(
        r'Take a break for (?P<number>[0-9]+) (?P<unit>\w+)s? '
        r'before trying again\.$',
        re.IGNORECASE)
    try:
        message.reply(truncate_body(body))
    except praw.exceptions.RedditAPIException as err:
        time_map = {
            'millisecond': 1e-3,
            'second': 1,
            'minute': 60,
            'hour': 60 * 60,
        }
        matches = re.search(_pattern, err.message)
        print(matches)
        if not matches:
            raise Exception(
                f'Unable to parse rate limit message {err.message!r}')
            return
        delay = (
            int(matches["number"]) * time_map[matches["unit"].rstrip("s")]
            + 1 * time_map[matches["unit"].rstrip("s")])
        print(
            f"ratelimit hit, message was {err}. Sleeping for {delay} seconds.")
        sleep_with_countdown(delay)
        message.reply(truncate_body(body))


def list_table(d, n=("GEMtleperson", "score"), aligners=None):
    nb_items = len(n)
    if aligners is None:
        aligners = ["l"] + ["r" for i in range(nb_items-1)]
    if len(aligners) != nb_items:
        raise RuntimeError(
            f"there should be as many aligners ({len(aligners)}) as there are "
            f"headings ({nb_items})")
    c = list()
    for item_id in range(nb_items):
        c.append(max(max(
            (len(f"{l[item_id]}") for l in d)), len(n[item_id])))

    def alignment(nb_chars, aligner):
        if aligner == 'l':
            return ':' + (nb_chars + 1) * '-'
        elif aligner == 'r':
            return (nb_chars + 1) * '-' + ':'
        else:
            raise RuntimeError("aligners should be 'l' or 'r', you "
                               f"specified '{aligner}'")

    def sig(aligner):
        if aligner == 'l':
            return '<'
        elif aligner == 'r':
            return '>'
        else:
            raise RuntimeError("aligners should be 'l' or 'r', you "
                               f"specified '{aligner}'")

    retval = list()
    retval.append("| " + " | ".join(
        (f"{ni:<{ci}}" for (ni, ci) in zip(n, c))) + " |")
    retval.append("|" + '|'.join([(alignment(c[i], aligners[i])) for
                                  i in range(nb_items)]) + "|")
    for item in d:
        retval.append("| " + " | ".join(
            (f"{ni:{sig(ai)}{ci}}" for (ni, ai, ci) in zip(
                item, aligners, c))) + " |")
    return "\n".join(retval)
