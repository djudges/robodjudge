#!/usr/bin/env python3
import praw
import re
from datetime import datetime, date

client_id = "robodjudge"
client_id = "AH0OfNhq0sklDdXWsIWkSA"
client_secret = "y8KdwzBPOExwZ_Yk1QZRQG1foyepWg"


re_title = re.compile(
    "(Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday) SOTD Thread.*")

hashtag = "#robodjudgebeta"

def fetch_subreddit(reddit_instance, name):
    return reddit_instance.subreddit(name)

def fetch_threads(subreddit, re_title, since, limit=300):
    for submission in subreddit.new(limit=limit):
        if ((datetime.fromtimestamp(submission.created_utc) >=
             datetime.combine(since, datetime.min.time())) and
            re_title.match(submission.title)):
            print(submission.title, datetime.fromtimestamp(submission.created_utc))
            yield submission

def fetch_toplevel_comments(thread, hashtag):
    for comment in thread.comments:
        if hashtag in comment.body.lower():
            yield comment

def main():
    reddit_instance = praw.Reddit(
        client_id=client_id,
        client_secret=client_secret,
        user_agent="my user agent",
    )
    name = "wetshaving"
    sub = fetch_subreddit(reddit_instance, name)
    threads = fetch_threads(sub, re_title, date(2021, 7, 15))

    # for comment in sub.stream.comments():
    #     print("{0.author} just submitted\n{0.body}".format(comment))
    for thread in threads:
        for comment in fetch_toplevel_comments(thread, hashtag):
            print()
            print(f"{comment.author} (id = {comment.author.id}) posted comment id {comment.id}")
            print(comment.body)
            print()


if __name__ == "__main__":
    main()
