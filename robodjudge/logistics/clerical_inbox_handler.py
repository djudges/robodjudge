#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   clerical_inbox_handler.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   16 Aug 2021

@brief  ClericalInboxHandler listens to incoming messages and formulates jobs 
        from them

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import multiprocessing as mp
import praw


class ClericalInboxHandler():
    def __init__(self, reddit):
        """
        Keyword Arguments:
        reddit     -- reddit instance
         """
        self.reddit = reddit
        self._prepped_message_generator = self._generator()

    def _generator(self):
        # I fetch the stream first, to avoid the unlikely situation where a
        # new message arrives between the fetch of the inbox and the stream
        # creation . This way, at worst, a message comes in twice
        def is_incoming(message):
            return message.dest == self.reddit.user.me()
        stream = self.reddit.inbox.stream(pause_after=2, skip_existing=True)

        # reverse would be more efficient, since these message come in
        # inverse chronological order, but I couldn't find that
        # statement in the doc, so I don't dare rely on it
        preexisting_messages = sorted(
            (msg for msg in self.reddit.inbox.messages(limit=1000)
             if is_incoming(msg)),
            key=lambda msg: msg.created_utc)

        for message in preexisting_messages:
            yield message

        message_stream = (message for message in stream
                          if message is None or
                          (isinstance(message, praw.models.Message)
                           and is_incoming(message)))
        for message in message_stream:
            yield message

    def __iter__(self):
        return iter(self._prepped_message_generator.__next__, None)
