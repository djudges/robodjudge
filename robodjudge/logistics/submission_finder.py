#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   submission_finder.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>xo

@date   02 Aug 2021

@brief  this class finds a specified number of submissions matching a pattern
        in a given time frame  and enqueues them in chronological order

Copyright © 2021 Djundjila

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from . import stop_iteration_tag
from .. import data
import praw

import multiprocessing as mp


class SubmissionFinder(mp.Process):
    def __init__(self, bot_conf, subreddit, patterns, submission_id_queue,
                 nb_submissions, since, pause_after=1, limit=500):
        """
        Keyword Arguments:
        bot_conf            -- name for the config in praw.ini to use,
                               read-only
        subreddit           -- name of the subreddit to trawl
        patterns            -- regular expression patterns to match the
        submission_id_queue -- output queue where the id's of the found
                               submissions are posted
        nb_submissions      -- number of submissions to expect before stopping
        since               -- datetime of cutoff point. Submissions older than
                               `since` will be disregarded
        pause_after         -- yield None if pause_after requests have not
                               yielded a new item
        limit               -- maximum number of sumbissions to check (should
                               never happen if number is chosen sufficiently
                               large)
        """
        super().__init__()
        self.reddit = (praw.Reddit(
            bot_conf,
            user_agent="RoboDjudge Submission Finder")
                       if isinstance(bot_conf, str) else bot_conf)
        self.subreddit = self.reddit.subreddit(subreddit)
        self.patterns = patterns
        self.submission_id_queue = submission_id_queue
        self.nb_submissions = nb_submissions
        self.nb_submissions_still_to_do = nb_submissions
        self.since = since.timestamp()
        self.pause_after = pause_after
        self.limit = limit
        self.prepped_generator = None

    def _generator(self):
        """
        Finds all existing threads matching self.patterns and keeps monitoring
        the sumbission stream of the subreddit until the self.nb_submissions
        have been found
        """
        # first find all matching existing Submissions
        existing_submissions = list()

        def validate_submission(submission):
            for pattern in self.patterns:
                if pattern.match(submission.title):
                    return True

        for submission in (submission for submission in
                           self.subreddit.new(self.limit) if
                           validate_submission(submission)):
            existing_submissions.append(submission)

        for submission in reversed(existing_submissions):
            yield submission
            self.nb_submissions_still_to_do -= 1
            if self.nb_submissions_still_to_do == 0:
                return

        # then, open a stream of new submissions and insert them as needed
        for submission in (
                submission for submission in
                self.subreddit.stream(
                    pause_after=self.pause_after).submissions() if
                validate_submission(submission)):
            yield submission
            if submission is not None:
                self.nb_submissions_still_to_do -= 1
                if self.nb_submissions_still_to_do == 0:
                    return
    def __iter__(self):
        if self.prepped_generator is None:
            self.prepped_generator = self._generator()
        return iter(self.prepped_generator.__next__, None)

    def run(self):
        for submission in self._generator():
            self.submission_id_queue.put(submission.id)
        self.submission_id_queue.put(stop_iteration_tag)
