#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   __init__.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>xo

@date   01 Aug 2021

@brief  this module implements rules by which RoboDjudge fetches posts, assigns
        them to djudges, and reads their djudgements

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from .queue_helper import existing_entries, stop_iteration_tag
from .clerical_inbox_handler import ClericalInboxHandler
from .submission_finder import SubmissionFinder
from .comment_harvester import DeprecatedCommentHarvester
from .message import MessageOrder
from .clerical_reminder import ClericalReminder
from .djudge_clerk import DjudgeClerk
from .post_fetcher import generate_AA_thread_dict, PostFetcherDict
