#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   djudge_clerk.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>xo

@date   03 Aug 2021

@brief  functor to fetch posts and invite judges to posts

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from datetime import datetime, timedelta
import time
import re
from collections import defaultdict

from .. import (data, exceptions, message_with_rate_limit,
                reply_with_rate_limit, list_table)
from . import stop_iteration_tag
from . import MessageOrder
from . import ClericalReminder, ClericalInboxHandler
from . import existing_entries


class DjudgeClericalError(exceptions.RoboDjudgeError):
    pass


class DjudgeClerk():
    def __init__(self, reddit, db_connection,
                 mum,
                 djudgement_class,
                 post_fetcher,
                 contest_name,
                 reminder_delay=timedelta(days=1),
                 message_rate=timedelta(seconds=.001),
                 wait_for_stop=False,
                 get_leaderboard=lambda x: None,
                 handle_djudge_assignment=None):
        """
        Keyword Arguments:
        reddi           -- praw reddit insance
        db_connection   --
        mum             -- username of admin to send complaints/errors/warnings
        djudgement_class--
        post_fetcher    -- object with method `fetch` to yield new posts and
                           `fetch_all` to refetch all posts, including old ones
        contest name    -- how you'd like the clerk to refer to this contest
        reminder_delay  -- (default timedelta(days)
        wait_for_stop   -- if yes, the clerk will keep working until getting a
                           'query stop' message from mum
        get_leaderboard -- a function which is called with the db connection
                           as argument and produces the leader board
        handle_djudge_assigment -- a function taking self, a post, and a list
                                   of djudges as argument and assigns djudges
                                   to the post
        """
        super().__init__()
        self.reddit = reddit
        self.mum = mum
        self.connection = db_connection
        self.reminder_clerk = ClericalReminder(self.connection, reminder_delay)
        self.contest_name = contest_name
        self.reminder_delay = reminder_delay
        self.djudgement_class = djudgement_class
        self.post_id_stopiteration = False
        self.re_djudgement = re.compile(
            ".*Inviting Djudge u/([A-Za-z_0-9]+) to djudge "
            "post id (.*)$")
        self.message_recipients = dict()
        self.message_rate = message_rate
        self.last_message_at = datetime.now() - self.message_rate
        self.post_fetcher = post_fetcher
        self.inbox_handler = ClericalInboxHandler(self.reddit)
        self.wait_for_stop = wait_for_stop
        self.get_leaderboard = get_leaderboard

        def noop(*args, **kwargs):
            pass

        # this is a callable that can be used for mocking internals during
        # testing
        self.hook = noop
        self.start_time = datetime.now()
        self.handle_djudge_assignment = (
            self.default_handle_djudge_assignment
            if handle_djudge_assignment is None
            else lambda post, djudges: handle_djudge_assignment(
                    self, post, djudges))

    def have_open_invitations(self):
        """
        return whether there are still outstanding djudgements
        """
        nb_open = data.DjudgeInvitation.count_open_invitations(self.connection)
        return bool(nb_open)

    def add_djudge(self, username):
        try:
            praw_djudge = self.reddit.redditor(username)
            if data.Djudge.exists(self.connection, praw_djudge):
                return f"Djudge u/{praw_djudge.name} is already active"
            else:
                djudge = data.Djudge.insert(self.connection, praw_djudge)
                self.invite_queries((djudge, ))
                for post in data.Post.all(self.connection):
                    self.invite_djudge(post, djudge)
                return f"Djudge u/{praw_djudge.name} has been invited"
        except Exception as err:
            msg = (f"Caught error '{err}' while trying to add "
                   f"Djudge {username}")
            print(msg)
            return msg

    def invite_djudge(self, post, djudge, invite=None):
        body = (f"Your Honour, u/{post.author.username} has posted for "
                f"{self.contest_name} in the '[{post.thread.title}]"
                f"({post.thread.permalink})' thread. "
                "Please read "
                f"[**the post**]({post.permalink}) and reply to this message "
                "with your djudgement in the following format:\n\n"
                f"{self.djudgement_class.instructions()}\n\n"
                "The whitespaces are not important (i.e., if you have "
                "spaces, tabs or newlines before or after the '::' and ';;'"
                " tokens, they are ignored. "
                "Please do not use the '::' and ';;' tokens in your "
                "comment. "
                "If your djudgement is not "
                "understood by my small bot brain, I will let you and "
                f"u/{self.mum} know and ask you to re-reply with a "
                "correctly formatted djudgement. Thank you for your service!")
        if invite is not None:
            if (invite.post.pk != post.pk or
                    invite.djudge.pk != djudge.pk):
                raise DjudgeClericalError(
                    "Inconsistency between"
                    f"post {post}, djudge {djudge} and invite {invite}")
        if djudge.pk != post.author.pk:
            invite = data.DjudgeInvitation.insert(
                self.connection, post, djudge) if invite is None else invite
            subject = (f"Inviting Djudge u/{djudge.username} to djudge "
                       f"post id {invite.safety_token}")
            self.handle_outbox_message(MessageOrder(
                recipient=djudge.username,
                subject=subject,
                body=body))
            self.reminder_clerk.put(invite.safety_token)
        else:
            self.handle_outbox_message(MessageOrder(
                recipient=djudge.username,
                subject="Notification of received post",
                body=(f"Your Honour, your [post]({post.permalink}) and"
                      " has been sent to your fellow djudges for "
                      "evaluation.")))

    def default_handle_djudge_assignment(self, post, djudges):
        for djudge in djudges:
            self.invite_djudge(post, djudge)

    def handle_new_post(self, comment):
        """
        1)
        create a WARNING message to mummy if the post already exists
        2)
        make sure the post's user and thread are in the db and store the post
        there as well
        3)
        create an invitation message to each djudge
        4)
        create a djudgement work order db entry
        5)
        create a reminder item

        Keyword Arguments:
        comment --
        """
        if data.Post.exists(self.connection, comment):
            post = data.Post.fetch(self.connection, comment)
            invites = data.DjudgeInvitation.invites_for_post(
                self.connection, post)
            if len(invites) == 0:
                body = (f"[this post]({comment.permalink}) ended up in the "
                        "DjudgeClerk's job queue twice but has not yet been"
                        "handled!")
                self.handle_outbox_message(MessageOrder(
                    recipient=self.mum,
                    subject="WARNING, duplicate post",
                    body=body))
            self.connection.commit()
            return
        else:
            if not data.Participant.exists(self.connection, comment.author):
                data.Participant.insert(self.connection, comment.author)
                self.rescan_harvested_threads_for(comment.author)
            else:
                post = data.Post.insert(self.connection, comment)
                djudges = data.Djudge.all(self.connection)
                if len(djudges) == 0:
                    raise DjudgeClericalError(
                        "You forgot to define djudges!")
                self.handle_djudge_assignment(post=post, djudges=djudges)

    def rescan_harvested_threads_for(self, praw_author):
        for post in self.post_fetcher.fetch_all():
            if post.author.name == praw_author.name:
                self.handle_new_post(post)

    def handle_new_djudgement(self, praw_message):
        """
        Keyword Arguments:
        djudgement_message --
        """
        try:
            djudge_name, safety_token = self.re_djudgement.match(
                praw_message.subject).groups()
        except AttributeError as err:
            raise AttributeError(
                f"{err}: encountered while trying to match subject "
                f"'{djudgement_message.subject}'")
        if praw_message.created_utc < self.start_time.timestamp():
            print("old (discarded)")
            return
        djudgement_message = data.Message.insert(self.connection, praw_message)
        print("parsed, 👍")
        try:
            corresponding_invitation = data.DjudgeInvitation.fetch(
                self.connection, safety_token)
            error_message = list()
            if djudge_name != djudgement_message.author.username:
                error_message.append(
                    "Djudgement "
                    f"coming from u/{djudgement_message.author.username} (according "
                    "to the message meta data should have come from "
                    f"'{djudge_name}' according to the subject clearname.")
            if djudge_name != corresponding_invitation.djudge.username:
                error_message.append(
                    "Possibly falsified djudgement message: "
                    "according to the database, the djudge name corresponding to "
                    "the safety token '{safety_token}' should be "
                    f"'{corresponding_invitation.djudge.username}', but the "
                    "subject line states '{djudge_name}'.")
            if (djudgement_message.author.username !=
                    corresponding_invitation.djudge.username):
                error_message.append(
                    "Possibly falsified djudgement message: "
                    "according to the database, the djudge name corresponding to "
                    "the safety token '{safety_token}' should be "
                    f"'{corresponding_invitation.djudge.username}', but the "
                    f"message came from '{djudgement_message.author.username}'.")
            if error_message:
                raise DjudgeClericalError(" ".join(
                    ["Inconsistent situation:"] + error_message))
            if corresponding_invitation.status == "djudged":
                djudgement = self.djudgement_class.fetch(
                    self.connection,
                    corresponding_invitation.post,
                    corresponding_invitation.djudge)
                djudgement.update(self.connection, djudgement_message)
                body = (
                    f"Your djudgement has been amended in the following way:"
                    f"\n\n{djudgement}\n\n"
                    "If this seems wrong to you, reply to this message with a "
                    "corrected djudgement or contact the botmaster")
                self.handle_reply(praw_message, body)
            else:
                djudgement = self.djudgement_class.insert(
                    self.connection,
                    corresponding_invitation.post,
                    corresponding_invitation.djudge,
                    djudgement_message)
                corresponding_invitation.set_djudged(self.connection)
                self.reminder_clerk.purge(
                    corresponding_invitation.safety_token)
                body = (
                    f"The following djudgement has been recorded:"
                    f"\n\n{djudgement}\n\n"
                    "If this seems wrong to you, reply to this message with a "
                    "corrected djudgement or contact the botmaster")
                self.handle_reply(praw_message, body)
        except (data.GradeError, ValueError) as err:
            error_message = f"{err}"
            body = ("my tiny bot brain could not comprehend your djudgement. "
                    f"My problem is: {error_message} "
                    "Please reformulate your djudgement following the rules I "
                    "wrote in the invitation. If you think I made a mistake, "
                    f"please contact botmaster u/{self.mum}")
            self.handle_reply(praw_message, body)
        except DjudgeClericalError as err:
            print(err)
            self.handle_reply(praw_message,
                              f"{err}")
        except data.DjudgeInvitationError as err:
            msg = (f"Caught the following error when treating "
                   f"your djudgement: '{err}'")
            print(msg)
            self.handle_reply(praw_message,
                              msg)

    def handle_new_reminder(self, safety_token):
        """
        Keyword Arguments:
        safety_token -- safety token of djudgement in question
        """
        invite = data.DjudgeInvitation.fetch(self.connection, safety_token)
        post = invite.post
        djudge = invite.djudge
        status = invite.status
        if status == "djudged":
            return  # nothing to do
        elif status == "invited":
            body = (f"Your Honour, this is a reminder to djudge "
                    f"u/{post.author.username}'s post for "
                    f"{self.contest_name}. Please read "
                    f"[the post]({post.permalink}) and reply to this message "
                    "with your djudgement in the following format:"
                    f"{self.djudgement_class.instructions()}"
                    "The whitespaces are not important (i.e., if you have "
                    "spaces, tabs or newlines before or after the '::' and ';;'"
                    " tokens, they are ignored. "
                    "Please do not use the '::' and ';;' tokens in your comment"
                    "If your djudgement is not "
                    "understood by my small bot brain, I will let you and "
                    f"u/{self.mum} know and ask you to re-reply with a "
                    "correctly formatted djudgement. Thank you for your "
                    "service!")
            subject = (f"REMINDER: Inviting Djudge u/{djudge.username} to "
                       f"djudge post id {invite.safety_token}")
            self.handle_outbox_message(MessageOrder(recipient=djudge.username,
                                                    subject=subject,
                                                    body=body))
            self.reminder_clerk.put(invite.safety_token)
            invite.set_reminded(self.connection)

        elif status == "reminded":
            subject = (f"Cancelled: Invitation to  djudge "
                       f"post id {invite.safety_token}")
            body = "Your judgement is no longer expected"

            self.handle_outbox_message(MessageOrder(recipient=djudge.username,
                                                    subject=subject,
                                                    body=body))
            invite.set_delinquent(self.connection)
        elif status == "delinquent":
            pass
        else:
            raise DjudgeClericalError(
                f"I don't know what to to with invitation '{invite}'")

    def list_invites(self, only_open=True, djudge=None):
        invites = list(data.DjudgeInvitation.fetch_open(self.connection)
                       if only_open
                       else data.DjudgeInvitation.all(self.connection))
        if djudge is not None:
            invites = [invite for invite in invites
                       if invite.djudge.pk == djudge.pk]

        if invites:
            rows = list()
            for inv in invites:
                day = datetime.fromtimestamp(
                    inv.post.created_utc).date().isoformat()
                rows.append((
                    (f"{inv.status}|u/{inv.djudge.username}"
                     f"|u/{inv.post.author.username}"
                     f"|[{day}]({inv.post.permalink})"
                     f"|{inv.safety_token}|"),
                    inv.post.created_utc))
            return ("|status|djudge|participant|date|safety_token|\n"
                    "|-|-|-|-|-|\n" +
                    '\n'.join(tup[0] for tup in
                              sorted(rows, key=lambda x: x[1])))
        else:
            return "There are no{} invites to list".format(
                " open" if only_open else "")

    def compile_participation_summary(self):
        posts = data.Post.all(self.connection)
        nb_harvested_threads = len(list(data.Thread.get_all(
            self.connection, harvested=True)))
        counter_submitted_dict = defaultdict(int)
        counter_disqualified_dict = defaultdict(int)
        participants = set()
        for post in posts:
            counter_submitted_dict[post.author.username] += 1
            participants.add(post.author)

        for participant in participants:
            post_disqualifications_dict = defaultdict(list)
            djudgements = self.djudgement_class.get_for_participant(
                self.connection, participant)
            for djudgement in djudgements:
                post_disqualifications_dict[djudgement.post.pk].append(
                    djudgement.grades['dq'].value)
            for post, dqs in post_disqualifications_dict.items():
                if all(dqs):
                    counter_disqualified_dict[participant.username] += 1
                    pass
                pass
            pass
        table = sorted((
            (f"u/{p.username}",
             f"{counter_submitted_dict[p.username]}/{nb_harvested_threads}",
             counter_disqualified_dict[p.username],
             (counter_submitted_dict[p.username] -
              counter_disqualified_dict[p.username]))
            for p in participants), key=lambda tup: (tup[-1], tup[0]))
        return list_table(table, n=("GEMtleperson", "submitted", "dqed", "total"))

    def handle_new_query(self, praw_message):
        """
        Keyword Arguments:
        query_message --
        """
        def inform(msg, body):
            if to_console:
                print(f"in response to query '{command}' by "
                      f"u/{praw_message.author.name}:")
                print(body)
            else:
                self.handle_reply(msg, body)

        if praw_message.created_utc < self.start_time.timestamp():
            print("old (discarded)")
            return
        query_message = data.Message.insert(self.connection, praw_message)
        is_djudge = data.Djudge.exists(self.connection, praw_message.author)
        is_admin = praw_message.author.name == self.mum.name
        praw_author = praw_message.author
        try:
            tokenized = praw_message.body.split('\n', maxsplit=1)
            if len(tokenized) == 2:
                command, payload = tokenized
            else:
                command, payload = tokenized + ['']
            command = command.lower()
            payload = payload.lower()
            to_console = "console" in command
        except Exception as err:
            msg = (f"Caught error '{err}' while trying to parse your message "
                   f"'{praw_message.body}.")
            print("ERROR:", msg)
            inform(praw_message,
                   msg)
        if "include post" in command:
            if not is_djudge:
                inform(praw_message,
                       "only djudges can do that")
            else:
                try:
                    url = payload
                    post = self.reddit.comment(url=url)
                    if data.Post.exists(self.connection, post):
                        inform(praw_message,
                               "post is already included")
                    else:
                        self.handle_new_post(post)
                        inform(
                            praw_message,
                            f"I've added u/{post.author.name}'s [post]"
                            f"({post.permalink}) in "
                            f"[{post.submission.title}]"
                            f"({post.submission.permalink}).")
                except Exception as err:
                    inform(
                        praw_message,
                        f"Caught error {err} while trying to add this post")
        elif "leaderboard" in command:
            if not is_djudge:
                inform(praw_message,
                       "only djudges can do that")
            else:
                leader_board = self.get_leaderboard(self.connection)
                inform(praw_message,
                       leader_board)

        elif "stop djudge" in command:
            if not is_admin:
                inform(praw_message,
                       "only admins can do that")
            else:
                self.wait_for_stop = False
                reasons = self.reasons_to_stay_alive()
                if reasons:
                    body = ("I've received your order to shut down, "
                            "but I'm waiting for the following reason: ")
                    body += " ".join(reasons)
                else:
                    body = "Shutting down"

                inform(praw_message, body)
        elif "list all invites" in command:
            if not is_admin:
                inform(praw_message,
                       "only admins can do that")
            else:
                body = self.list_invites(only_open=False)
                inform(praw_message, body)
        elif "list all open invites" in command:
            if not is_admin:
                inform(praw_message,
                       "only admins can do that.")
            else:
                body = self.list_invites(only_open=True)
                inform(praw_message, body)
        elif "list my open invites" in command:
            if not is_djudge:
                inform(praw_message,
                       "only djudges can do that")
            else:
                body = self.list_invites(only_open=True,
                                         djudge=query_message.author)
                inform(praw_message, body)
        elif "list all my invites" in command:
            if not is_djudge:
                inform(praw_message,
                       "only djudges can do that")
            else:
                body = self.list_invites(only_open=False,
                                         djudge=query_message.author)
                inform(praw_message, body)
        elif "add djudge" in command:
            if not is_admin:
                inform(praw_message,
                       "only admins can do that")
            else:
                body = self.add_djudge(payload.strip())
                inform(praw_message, body)
        elif "resend open invites" in command:
            if not is_djudge:
                inform(praw_message,
                       "only djudges can do that")
            else:
                invites = [invite for invite in
                           data.DjudgeInvitation.fetch_open(self.connection)
                           if invite.djudge.username == praw_author.name]
                if not invites:
                    body = "You don't have any open invitations, good job!"
                else:
                    try:
                        total_num = len(invites)
                        nb_invites = int(payload) if payload else total_num
                        nb_todo = total_num - nb_invites
                        invites = invites[:nb_invites]

                        for invite in invites:
                            self.invite_djudge(invite.post,
                                               invite.djudge,
                                               invite)

                            body = ("Done, you should have received "
                                    f" {len(invites)} invites.")
                            if payload:
                                body += (
                                    f" After this, you have {nb_todo} "
                                    "djudgements left to do.")
                    except Exception as err:
                        body = (
                            "Couldn't understand the payload of your request: "
                            f" what is '{payload}'?")
                inform(praw_message, body)
        elif "participation summary" in command:
            body = self.compile_participation_summary()
            inform(praw_message, body)
        else:
            subject = f"Incoming query from u/{query_message.author.username}"
            body = (f"Subject was '{query_message.subject}'\n\n"
                    f"Body was\n\n{query_message.body}")
            self.handle_outbox_message(MessageOrder(recipient=praw_author.name,
                                                    subject=subject,
                                                    body=body))

    def handle_outbox_message(self, message_order):
        """
        """
        dest_username = message_order.recipient
        if dest_username not in self.message_recipients.keys():
            dest = self.reddit.redditor(dest_username)
            self.message_recipients[dest_username] = dest
        else:
            dest = self.message_recipients[dest_username]
        now = datetime.now()
        wait_time = max(timedelta(seconds=0.),
                        (self.last_message_at + self.message_rate) - now)
        print(f"Trying to send message '{message_order.subject}'")
        time.sleep(wait_time.total_seconds())
        self.last_message_at = datetime.now()
        message_with_rate_limit(dest,
                                message_order.subject,
                                message_order.body)

    def handle_reply(self, praw_message, body):
        """
        """
        now = datetime.now()
        wait_time = max(timedelta(seconds=0.),
                        (self.last_message_at + self.message_rate) - now)
        time.sleep(wait_time.total_seconds())
        print(f"Trying to send reply '{praw_message.subject}'")
        self.last_message_at = datetime.now()
        reply_with_rate_limit(praw_message, body)

    def handle_incoming_message(self, praw_message):
        # check each type of possible message I'm expecting
        print(f"Got message at {datetime.now().isoformat(sep=' ')} from "
              f"u/{praw_message.author.name} with subject "
              f"'{praw_message.subject}")
        if self.re_djudgement.match(praw_message.subject):
            print("identified as djudgement... ",end="")
            # I got a djudgement back
            self.handle_new_djudgement(praw_message)
        elif re.compile(".*query.*", re.IGNORECASE).match(praw_message.subject):
            print("identified as query... ",end="")
            self.handle_new_query(praw_message)
        else:
            if praw_message.created_utc > self.start_time.timestamp():
                error_msg = "could not identify this message"
                self.handle_reply(praw_message, error_msg)
                print(error_msg)

    def reasons_to_stay_alive(self):
        reasons = list()
        if self.wait_for_stop:
            reasons.append(
                "I've been told to wait for the stop signal from an admin.")
        if self.have_open_invitations():
            reasons.append(
                "I'm still waiting for djudgements.")
        if not self.reminder_clerk.empty():
            reasons.append("I still have open reminders running.")
        if list(data.Acknowledgement.get_unacknowledged_posts(self.connection,
                                                              limit=1)):
            reasons.append("I still have posts to acknowledge.")
        return reasons

    def acknowledge_post(self, post):
        body = """
I am u/RoboDjudge, your friendly robotic djudge clerk, and I'm writing to
confirm that this SOTD has been submitted for djudgement to our expert panel
of djudges. (u/djundjila, u/VisceralWatch, u/Semaj3000, and u/EldrormR)

Because I'm a young bot I still make mistakes, and if any of your SOTDs has not
received this message, please let one of the djudges know in a PM so they can
add it. **You have until Sat Sep 4, 23:59 PST to appeal** before we tally the
final scores for the GEMs of Wisdom: Finding Serenity in Austerity Side Side
Challenge.

Thank you very much for your participation!

PS: **Please consider upvoting this comment**, because I need karma to reduce
reddit's rate limit which hampers my functions."""
        with self.connection:
            print(f"Acknowledging post by u/{post.author} in thread "
                  f"{post.thread.title}")
            data.Acknowledgement.insert(self.connection, post)
            reply_with_rate_limit(post.get_praw(self.reddit), body)

    def invite_queries(self, djudges=None):
        if djudges is None:
            djudges = data.Djudge.all(self.connection)
        subject = "how to query the RoboDjudge"
        body = f"""Query Power!

RoboDjudge can return information to queries asked by participants or djudges
by writing hir/hem a PM with the word 'query' in the title or by responding to
this message (The latter should be your preferred option as it keeps your inbox
less cluttered). In a query message, the first line of the text is considered
the command, and the subsequent lines are additional information some queries
need.

* **Example 1 - the Score:** if you want to know the current leader board, you
can simply reply '**leaderboard**' to this message and RoboDjudge will compile
the scores and send them back in a neat table.

* **Example 2 - Adding a Post:** If u/Gold_Biker forgot to post on time and you
(a Djudge) make the decision to add the post anyway, reply '**include post**'
and the **post's permalink** on two lines (the words 'include post' on the first
line of your response, and the permalink/url/link on the second line)

* **Example 3 - Help! I lost sight of the invitation:** Don't panic, just reply
'**list my open invites** to get a list of all your posts still to djudge (you
can also get a list of all invites regardless of status with
'**list my invites**'. You want robodjudge to resend open invitations?
Simply reply '**resend open invites**' and RoboDjudge has your back.

There are more queries and u/{self.mum} can add some if the are simple enough
to implement, just let them know.

RoboDjudge replies to every query but reddit has strict rate-limiting rules for
outgoing messages. Especially right after a new thread can be harvested and
Robodjudge sends out whole bunch of messages, you might have to wait a few
minutes before hearing back. The vast majority of messages will be answered
within seconds. If you don't hear back for over half and hour, it means that
RoboDjudge crashed. Try again later and inform u/{self.mum} if you're in a
hurry.

RoboDjudge is still evolving a lot, so please be patient with me and don't get
upset if you receive this message multiple times. As the software matures, this
issue will resolve itself.  """
        for djudge in djudges:
            try:
                msg_order = MessageOrder(recipient=djudge.username,
                                         subject=subject,
                                         body=body)
            except TypeError as err:
                raise err
            self.handle_outbox_message(msg_order)

    def run(self):
        """
        picks items off the job_queue queue, and handles
        them as follows:
        post_id: * check whether this post has already been treated (this is a
                   redundant check)
                 * create an invitation message per judge and put in the
                   messaging queue
                 * create a db entry for and undjudged djudgement for each
                   djudge
                 * put a reminder item into the reinder queue to be checked
                   and potentially resent to delinquent djudges after
                   reminder_delay
        djudgement: * cancel the undjudged djudgement
                    * store the djudgement
        reminder: * check whether the undjudged djudgement is still in the db
                    (this is a redundant check)
                  * create a reminder message and put it in the messaging queue
                  * put a reminder item into the reminder queue to be checked
                    and potentially resent to be recidivist djudge
        """
        # determine whether it's necessary to emit invites
        if not data.Participant.all(self.connection):
            self.invite_queries()

        while (True):

            for post in self.post_fetcher.fetch():
                self.handle_new_post(post)

            for safety_token in self.reminder_clerk.get():
                self.handle_new_reminder(safety_token)

            self.hook(self)

            inbox_iter = list(self.inbox_handler.__iter__())
            for message in inbox_iter:
                self.handle_incoming_message(message)

            for post in data.Acknowledgement.get_unacknowledged_posts(
                    self.connection, limit=1):
                self.acknowledge_post(post)

            if not self.reasons_to_stay_alive():
                break
