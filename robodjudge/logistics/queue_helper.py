#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   queue_helper.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   20 Aug 2021

@brief  Little helpers with using mutiple queues in parallel

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import queue

stop_iteration_tag = "STOP"


def existing_entries(q):
    try:
        for entry in iter(q.get_nowait, None):
            yield entry
    except queue.Empty:
        pass
