#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   post_fetcher.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   22 Aug 2021

@brief  Simple function looking for threads and fetching their posts if it's
        time

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from datetime import datetime, time, date
import pytz

from .comment_harvester import harvest_comments
from ..data import Thread


def generate_AA_thread_dict(year=2021, deadline_time=time(23, 59), days=0,
                            timezone=pytz.timezone("US/Pacific")):
    """
    year = calendar year for which to compute the dict
    deadline_time = time at which the thread can be harvested (pencils down!)
    days = how many days later the harvest happens (usually 0)
    timezone = timezone in which the deadline is specified
    """
    def deadline_and_title():
        for day in range(1, 32):
            deadline = timezone.localize(
                datetime.combine(date(year, 8, day),
                                 deadline_time))
            title = deadline.strftime(
                "%A Austere August SOTD Thread - %b %d, %Y")
            yield title, deadline
    return {title: deadline for (title, deadline) in deadline_and_title()}


class PostFetcherDict():

    def __init__(self, thread_dict, subreddit, connection, since,
                 posts_filter):
        """
        Keyword Arguments:
        thread_dict  -- a dict matching thread title patterns to harvesting
                        times
        subreddit    -- the subreddit's name that should be trawled
        since        -- how far back in the past to go look
        posts_filter -- a callable that takes a sequence of posts and returns a
                        generator yielding only the ones we're interested in
        """
        self.thread_dict = thread_dict
        self.subreddit = subreddit
        self.connection = connection
        self.since = since
        self.posts_filter = posts_filter
        self.harvested_threads = {thread.title for thread in
                                  Thread.get_all(connection, True)}

    def fetch_all(self):
        for submission in self.subreddit.new(limit=1000):
            if submission.created_utc < self.since:
                break

            if (submission.title in self.thread_dict.keys()):
                if not Thread.exists(self.connection, submission):
                    thread = Thread.insert(self.connection, submission)
                else:
                    thread = Thread.fetch(self.connection, submission)
                deadline = self.thread_dict[submission.title]
                if datetime.now(deadline.tzinfo) > deadline:
                    for post in self.harvest(submission, thread,
                                             mark_harvested=False):
                        yield post

    def fetch(self):
        for submission in self.subreddit.new(limit=1000):
            if submission.created_utc < self.since:
                break

            if (submission.title in self.thread_dict.keys() and
               submission.title not in self.harvested_threads):
                if not Thread.exists(self.connection, submission):
                    thread = Thread.insert(self.connection, submission)
                else:
                    thread = Thread.fetch(self.connection, submission)
                deadline = self.thread_dict[submission.title]
                if datetime.now(deadline.tzinfo) > deadline:
                    for post in self.harvest(submission, thread):
                        yield post

    def harvest(self, submission, thread, mark_harvested=True):
        for comment in harvest_comments(submission, self.posts_filter):
            yield comment
        if mark_harvested:
            thread.set_harvested(self.connection)
            self.harvested_threads.add(submission.title)
