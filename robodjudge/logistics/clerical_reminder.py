#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   clerical_reminder.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   14 Aug 2021

@brief  Defines the ClericalReminder class that reminds the DjudgeClerk to
        check up on invitation statuses

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import multiprocessing as mp
from datetime import datetime, timedelta
import time

from collections import namedtuple

from . import stop_iteration_tag
from ..data import Reminder


class ClericalReminder():
    def __init__(self, connection, default_delay=timedelta(days=1)):
        self.connection = connection
        self.default_delay = default_delay
        self.Reminder = namedtuple("Reminder", ("time", "payload"))

    def empty(self):
        return not bool(Reminder.all(self.connection))

    def purge(self, purge_payload):
        Reminder.purge(self.connection, purge_payload)

    def put(self, payload, delay=None):
        if delay is None:
            delay = self.default_delay
        now = datetime.now()
        reminder_time = now + delay
        if Reminder.exists(self.connection, payload):
            self.purge(payload)
        Reminder.insert(self.connection, payload, reminder_time)

    def get(self):
        now = datetime.now()
        for i, reminder in enumerate(Reminder.all(self.connection)):
            if now < reminder.reminder_datetime:
                return
            return_value = reminder.safety_token
            reminder.delete(self.connection)
            yield return_value
