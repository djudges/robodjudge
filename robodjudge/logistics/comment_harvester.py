#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   comment_harvester.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>xo

@date   01 Aug 2021

@brief  functor to harvest posts, filter them by tag and put them in the post
        queue

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import multiprocessing as mp
import time
from datetime import timedelta, datetime
import praw

from .. import fetch_toplevel_comments
from . import stop_iteration_tag


def harvest_comments(thread, posts_filter):
    """
    Keyword Arguments:
    thread       -- a praw Submission instance
    posts_filter -- a callable that takes a sequence of posts and returns a
                    generator yielding only the ones we're interested in
    """
    toplevel_comments = fetch_toplevel_comments(thread)
    for praw_post in posts_filter(toplevel_comments):
        yield praw_post


class DeprecatedCommentHarvester(mp.Process):
    def __init__(self, bot_conf, submission_id_queue, post_id_queue,
                 messaging_queue, posts_filter,
                 delay_time=(
                     lambda thread: (max(
                         (datetime.fromtimestamp(thread.created_utc) +
                          timedelta(days=1, hours=1) -
                          datetime.now()).total_seconds(), 0)))):
        """
        Keyword Arguments:
        bot_conf        -- name for the config in praw.ini to use, read-only
        submission_id_queue -- input queue spitting out threads
                               (`submission_id`s)
        post_id_queue   -- output queue taking the posts
        messaging_queue -- queue to put emergency messages in
        posts_filter    -- callable that takes a sequence of posts and returns
                           a generator yielding only the ones we're interested
                           in
        delay_time      -- function returning the time to wait in seconds given
                           the thread  as input posts (default = 24 hours)
        """
        super().__init__()
        self.reddit = praw.Reddit(bot_conf, user_agent="RoboDjudge Harvester")
        self.thread_id_queue = submission_id_queue
        self.post_id_queue = post_id_queue
        self.messaging_queue = messaging_queue
        self.posts_filter = posts_filter
        self.delay_time = delay_time

    def run(self):
        """
        takes threads out of the thread_id_queue, waits until they are ripe and
        thes hardvests the posts. This relies on threads being put into the
        queue in chronological order
        """
        for thread_id in iter(self.thread_id_queue.get, stop_iteration_tag):
            thread = self.reddit.submission(thread_id)

            start_time = thread.created_utc
            wait_time = self.delay_time(thread)

            time.sleep(wait_time)

            for post in harvest_comments(thread, self.posts_filter):
                self.post_id_queue.put(("post_id", post.id))

            self.thread_id_queue.task_done()
        self.thread_id_queue.task_done() # for the stop_iteration_tag
        self.post_id_queue.put(stop_iteration_tag)
