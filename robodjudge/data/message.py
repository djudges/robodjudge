#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   message.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   20 Aug 2021

@brief  db storage for praw messages

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from .. import exceptions
from . import User
from datetime import datetime


class MessageError(exceptions.RoboDjudgeError):
    pass


class Message():
    tablename = "messages"

    def __init__(self, author, dest, subject, body, created_utc, message_id,
                 pk):
        self.author = author
        self.dest = dest
        self.subject = subject
        self.body = body
        self.created_utc = created_utc
        self.message_id = message_id
        self.pk = pk

    def __repr__(self):
        ret_val = list()
        ret_val.append(
            f"Message from u/{self.author.username} ⇒ u/{self.dest.username}")
        ret_val.append(
            f"On {datetime.fromtimestamp(self.created_utc).strftime('%c')}")
        ret_val.append(f"Subject: '{self.subject}'")
        ret_val.append(f"{self.body}")
        return "\n".join(ret_val)

    @classmethod
    def insert_manual(cls, connection, author_pk, dest_pk, subject,
                      body, created_utc, message_id):
        with connection:
            cursor = connection.cursor()
            command = (
                f"INSERT INTO {cls.tablename} "
                f"(author_pk, dest_pk, subject, body, created_utc, message_id) "
                f"VALUES (?, ?, ?, ?, ?, ?)")
            args = (author_pk, dest_pk, subject, body, created_utc, message_id)
            try:
                cursor.execute(command, args)
            except Exception as err:
                raise type(err)(
                    f"Caught '{err}' with args '{args}'")
            pk = cursor.lastrowid
        return cls(User.select(connection, author_pk),
                   User.select(connection, dest_pk),
                   subject, body, created_utc, message_id, pk)

    @classmethod
    def insert(cls, connection, praw_message):
        author = (User.fetch(connection, praw_message.author)
                  if User.exists(connection, praw_message.author)
                  else User.insert(connection, praw_message.author, []))
        dest = (User.fetch(connection, praw_message.dest)
                if User.exists(connection, praw_message.dest)
                else User.insert(connection, praw_message.dest, []))
        return cls.insert_manual(connection,
                                 author.pk,
                                 dest.pk,
                                 praw_message.subject,
                                 praw_message.body,
                                 praw_message.created_utc,
                                 praw_message.id)

    @classmethod
    def exists(cls, connection, praw_message):
        cursor = connection.cursor()
        command = (f"SELECT count(*) FROM {cls.tablename} "
                   "WHERE message_id=?")
        cursor.execute(command, (praw_message.id,))
        return cursor.fetchone()[0] == 1

    @classmethod
    def select(cls, connection, pk):
        cursor = connection.cursor()
        command = (
            f"SELECT author_pk, dest_pk, subject, body, created_utc, "
            f"message_id FROM {cls.tablename} "
            "WHERE pk=?")
        cursor.execute(command, (pk,))
        (author_pk, dest_pk, subject, body, created_utc,
         message_id) = cursor.fetchone()
        return cls(author_pk, dest_pk, subject, body, created_utc,
                   message_id, pk)

    @classmethod
    def fetch(cls, connection, praw_message):
        cursor = connection.cursor()
        command = (
            f"SELECT author_pk, dest_pk, subject, body, created_utc, "
            f"message_id, pk FROM {cls.tablename} "
            "WHERE message_id=?")
        cursor.execute(command, (praw_message.id,))
        (author_pk, dest_pk, subject, body, created_utc,
         message_id, pk) = cursor.fetchone()
        return cls(author_pk, dest_pk, subject, body, created_utc,
                   message_id, pk)


    @classmethod
    def create_table(cls, connection):
        with connection:
            cursor = connection.cursor()
            command = (f"CREATE TABLE {cls.tablename} "
                       "(pk INTEGER PRIMARY KEY AUTOINCREMENT, "
                       "author_pk INTEGER NOT NULL, "
                       "dest_pk INTEGER NOT NULL, "
                       "subject TEXT NOT NULL, "
                       "body TEXT NOT NULL, "
                       "created_utc INTEGER NOT NULL, "
                       "message_id TEXT UNIQUE NOT NULL, "
                       f"FOREIGN KEY (author_pk) REFERENCES "
                       f"{User.tablename}, "
                       f"FOREIGN KEY (dest_pk) REFERENCES "
                       f"{User.tablename}, "
                       f"CHECK (author_pk != dest_pk));")
            cursor.execute(command)

