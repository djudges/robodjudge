#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   __init__.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   30 Jul 2021

@brief  module for database representations of contest data

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from .user import Participant, User, Djudge, UserError
from .post import Post
from .acknowledgement import Acknowledgement
from .thread import Thread
from .message import Message, MessageError

from .score import (NumericGradeFactory,
                    BooleanGradeFactory,
                    TextGradeFactory,
                    DjudgementFactory,
                    GradeError)

from .djudge_invitation import DjudgeInvitation, DjudgeInvitationError
from .reminder import Reminder
