#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   score.py

@author Till Junge <djundjila.gitlab@cloudmail.altermail.ch>

@date   29 Jul 2021

@brief  simple classes to represent a djudge score

Copyright © 2021 Till Junge

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from .. import exceptions

from . import Participant, Thread, Djudge, User, Post


class GradeError(exceptions.RoboDjudgeError):
    pass


class GradeType():
    def __init__(self, default):
        """
        Keyword Arguments:
        default -- possible default value
        """
        self.default = default

    @property
    def is_optional(self):
        return self.default is None


class NumericGradeFactory():
    def __init__(self, minimum=0., maximum=5., default_=None):
        """
        Keyword Arguments:
        minimum -- (default_ 0.) min allowed grade
        maximum -- (default 5.) max allowed grade
        default_ -- (default None) optional default value
        """
        class NumericGradeClass(GradeType):
            has_default = default_ is not None
            data_type = "REAL NOT NULL"
            default = default_
            def __init__(self, value=default_):
                super().__init__(default_)
                value = float(value)
                if not (minimum <= value <= maximum):
                    raise GradeError(
                        f"the value {value} is not in the range "
                        f"[{minimum}, {maximum}]")
                self.value = value
                self.minimum = minimum
                self.maximum = maximum
            def __repr__(self):
                    return (
                        f"{self.value} ∈ [{self.minimum}, {self.maximum}]" +
                        (f" (default: {self.default})" if self.default is not None
                         else ""))

            @classmethod
            def instruction_line(cls, name):
                default_str = f" (default is '{default_}')" if cls.has_default else ""
                return (f"`{name} :: <Value ∈ [{minimum}, "
                        f"{maximum}] (bounds included){default_str}> ;;`  ")

            @classmethod
            def example_line(cls, name):
                val = .3 * minimum + .7 * maximum
                return (f"`{name} :: {val} ;;`  ")

        self.cls = NumericGradeClass


class BooleanGradeFactory():
    def __init__(self, default_=None):
        """
        Keyword Arguments:
        default_ -- (default None) default value (if this is optional)
        """
        class BooleanGrade(GradeType):
            has_default = default_ is not None
            data_type = "INTEGER NOT NULL"
            default = default_
            def __init__(self, value=default_):
                super().__init__(default_)
                if isinstance(value, bool):
                    self.value = value
                elif isinstance(value, str):
                    if value.strip().lower() == 'yes':
                        self.value = True
                    elif value.strip().lower() == 'no':
                        self.value = False
                    else:
                        raise Exception("please supply 'yes' or 'no'")
                elif isinstance(value, int):
                    self.value = value != 0
                else:
                    raise Exception(
                        "please supply 'yes', 'no', or a boolean, rather than "
                        f"'{value}'")


            @classmethod
            def instruction_line(cls, name):
                default_str = f" (default is '{default_}')" if cls.has_default else ""
                return (f"`{name} :: <'yes' or 'no'{default_str}> ;;`  ")

            @classmethod
            def example_line(cls, name):
                return (f"`{name} :: no ;;`  ")

            def __repr__(self):
                return (
                    f"{self.value}" +
                    (f" (default: {self.default})" if self.default is not None
                     else ""))

        self.cls = BooleanGrade

class TextGradeFactory():
    def __init__(self, default_=None):
        """
        Keyword Arguments:
        default_ -- (default None) default value (if this is optional)
        """
        class TextGrade(GradeType):
            has_default = default_ is not None
            data_type = "TEXT NOT NULL"
            default = default_
            def __init__(self, value=default_):
                super().__init__(default_)
                self.value = value

            @classmethod
            def instruction_line(cls, name):
                default_str = f" (default is '{default_}')" if cls.has_default else ""
                return (f"`{name} :: <free text without the tokens '::' or ';;'"
                        f"{default_str}> ;;`  ")

            @classmethod
            def example_line(cls, name):
                return (f"`{name} :: Vtephen_jk and Witchy_blooper ;;`  ")

            def __repr__(self):
                return (
                        f"'''{self.value}'''" +
                        (f" (default: '''{self.default}''')" if self.default is not None
                         else ""))
                return (f"A text grade" +
                        (f" with '{self.default}' as default value" if self.default is not None
                        else ""))
        self.cls = TextGrade

class DjudgementError(exceptions.RoboDjudgeError):
    pass

class DjudgementFactory():
    table_name = "djudgements"
    def __init__(self, name_type_dict_):
        """
        Keyword Arguments:
        name_type_dict  -- dictionary containing grade fields and types
        """
        for key in name_type_dict_.keys():
            if key.lower() != key:
                raise Exception(
                    f"Please use only lower case grade categories. this makes "
                    f"it easier to avoid case sensitivity. The offending "
                    f"category is '{key}'")
        class Djudgement():
            tablename = DjudgementFactory.table_name
            name_type_dict = name_type_dict_

            def __init__(self, post, djudge, pk,
                         **kwargs):
                """
                Keyword Arguments:
                djudge       -- djudge
                post         -- post
                kwargs**     -- dictionary containing grade fields and values
                """
                self.grades = dict()
                for key, constructor in self.name_type_dict.items():
                    if key not in kwargs:
                        if constructor.is_optional:
                            self.grades[key] = constructor()
                        else:
                            raise DjudgementError(
                                f"The grade category '{key}' was missing "
                                f"(type: '{constructor}')")
                    else:
                        self.grades[key] = constructor(kwargs[key])
                self.participant = post.author
                self.djudge = Djudge.check_role(djudge)
                self.post = post
                self.daily_thread = self.post.thread
                self.pk = pk

            def __repr__(self):
                out_msg = list()
                out_msg.append(
                    f"Djudge u/{self.djudge.username} gave the following scores:\n")
                for category, grade in self.grades.items():
                    out_msg.append(
                        f"* {category}: {grade}")
                out_msg.append(
                    f"\nfor [this post]({self.post.permalink}) by "
                    f"u/{self.participant.username} in thread "
                    f"[{self.post.thread.title}]({self.post.thread.permalink}).")
                return "\n".join(out_msg)

            @classmethod
            def parse_custom_field_kwargs(cls, **kwargs):
                grades = dict()
                for key, constructor in cls.name_type_dict.items():
                    if key not in kwargs:
                        if constructor.is_optional:
                            grades[key] = constructor().value
                        else:
                            raise DjudgementError(
                                f"The grade category '{key}' was missing "
                                f"(type: '{constructor}')")
                    else:
                        grade = constructor(kwargs[key]).value
                        grades[key] = grade
                return grades

            @classmethod
            def instructions(cls):
                ret_val = list()
                for key, constructor in cls.name_type_dict.items():
                    ret_val.append(
                        f"{constructor.instruction_line(key)}")
                ret_val.append("\nThis could for instance look like this:\n")
                for key, constructor in cls.name_type_dict.items():
                    ret_val.append(
                        f"{constructor.example_line(key)}")

                if any((constructor.has_default for _, constructor
                        in cls.name_type_dict.items())):
                    ret_val.append(
                        "Categories with a default value can be omitted.")
                return "\n".join(ret_val)

            @classmethod
            def parse_djudgement(cls, djudgement_message):
                key_set = set((key.lower() for key in cls.name_type_dict.keys()))
                used_keys = set()
                kwargs = dict()
                for item in djudgement_message.body.split(';;'):
                    if item.strip():
                        key, value_str = (val.strip()
                                          for val
                                          in item.split("::"))
                        key = key.lower()
                        if key not in key_set:
                            raise GradeError(
                                f"The grade category '{key}' does not exist in "
                                "this contest.")
                        if key in used_keys:
                            raise GradeError(
                                f"The grade category '{key}' has been mentioned"
                                " more than once.")
                        used_keys.add(key)
                        kwargs[key] = cls.name_type_dict[key](value_str).value
                unused_keys = key_set - used_keys
                for key in unused_keys:
                    if cls.name_type_dict[key].has_default:
                        constructor = cls.name_type_dict[key]
                        kwargs[key] = constructor(constructor.default).value
                    else:
                        raise GradeError(
                            f"the category '{key}' is missing in your "
                            "djudgement, but it's a mandatory category")
                return kwargs

            @classmethod
            def insert(cls, connection, post, djudge, message=None,
                       **kwargs):
                if message:
                    grades = cls.parse_djudgement(message)
                else:
                    grades = cls.parse_custom_field_kwargs(**kwargs)

                djudge = Djudge.check_role(djudge)
                with connection:
                    cursor = connection.cursor()
                    sorted_keys = sorted(cls.name_type_dict.keys())
                    command = (
                        f"INSERT INTO {cls.tablename} "
                        "(post_pk, djudge_pk, " +
                        ", ".join(sorted_keys) +
                        ") Values (" +
                        ", ".join(("?" for _ in range (
                            2 + len(cls.name_type_dict)))) +
                        ")")
                    grades_tuple = tuple((grades[key] for key in sorted_keys))

                    cursor.execute(
                        command,
                        (post.pk, djudge.pk) + grades_tuple)

                return cls(post, djudge,
                           cursor.lastrowid, **grades)

            def update(self, connection, message):
                new_grades = self.parse_djudgement(message)
                sorted_keys = sorted(self.name_type_dict.keys())
                with connection:
                    cursor = connection.cursor()
                    command = (
                        f"UPDATE {self.tablename} "
                        "SET " +
                        ", ".join((f'{key} = ?' for key in sorted_keys)) +
                        "WHERE pk = ?;")
                    grades_tuple = tuple((new_grades[key]
                                          for key in sorted_keys))

                    cursor.execute(
                        command,
                        grades_tuple + (self.pk, ))
                    for key in sorted_keys:
                        self.grades[key] = new_grades[key]


            @classmethod
            def get_for_participant(cls, connection,  participant):
                cursor = connection.cursor()
                sorted_keys = sorted(cls.name_type_dict.keys())
                command = (
                    f"SELECT {cls.tablename}.pk, post_pk, djudge_pk, " +
                    ", ".join(sorted_keys) +
                    f" FROM {cls.tablename}, {Post.tablename} "
                    f" WHERE post_pk = {Post.tablename}.pk "
                    f" AND {Post.tablename}.author_pk = ?")
                cursor.execute(command, (participant.pk, ))

                return [cls(Post.select(connection, post_pk),
                            Djudge.select(connection, djudge_pk),
                            pk,
                            **{key: val for (key, val)
                               in zip(sorted_keys, grades)})
                        for (pk, post_pk, djudge_pk, *grades) in
                        cursor.fetchall()]

            @classmethod
            def select(cls, connection, pk):
                cursor = connection.cursor()
                sorted_keys = sorted(cls.name_type_dict.keys())
                command = (
                    f"SELECT pk, post_pk, djudge_pk, " +
                    ", ".join(sorted_keys) +
                    f" FROM {cls.tablename} "
                    " WHERE pk = ?")
                cursor.execute(command, (pk, ))
                ret_pk, post_pk, djudge_pk, *grades = cursor.fetchone()
                return cls(Post.select(connection, post_pk),
                           Djudge.select(connection, djudge_pk),
                           ret_pk,
                           **{key: val for (key, val)
                              in zip(sorted_keys, grades)})

            @classmethod
            def fetch(cls, connection, post, djudge):
                cursor = connection.cursor()
                sorted_keys = sorted(cls.name_type_dict.keys())
                command = (
                    f"SELECT pk, post_pk, djudge_pk, " +
                    ", ".join(sorted_keys) +
                    f" FROM {cls.tablename} "
                    " WHERE post_pk = ? AND djudge_pk = ?")
                cursor.execute(command, (post.pk, djudge.pk))
                ret_pk, post_pk, djudge_pk, *grades = cursor.fetchone()
                return cls(Post.select(connection, post_pk),
                           Djudge.select(connection, djudge_pk),
                           ret_pk,
                           **{key: val for (key, val)
                              in zip(sorted_keys, grades)})

            @classmethod
            def create_table(cls, connection):
                with connection:
                    cursor = connection.cursor()

                    def format_customs(key, value):
                        appendix = "" if value.is_optional else " NOT NULL"
                        return f"{key} {value.data_type}" + appendix

                    command = (
                        f"CREATE TABLE {cls.tablename} "
                        "(pk INTEGER PRIMARY KEY AUTOINCREMENT, "
                        "post_pk INTEGER NOT NULL, "
                        "djudge_pk INTEGER NOT NULL, " +
                        ", ".join((format_customs(key, value) for (key, value) in
                                   cls.name_type_dict.items())) +
                        f", UNIQUE  (post_pk, djudge_pk), "
                        f"FOREIGN KEY (djudge_pk) REFERENCES {User.tablename} (pk), "
                        f"FOREIGN KEY (post_pk) REFERENCES {Post.tablename} (pk));")
                    cursor.execute(command)

        self.cls = Djudgement
