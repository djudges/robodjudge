#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   user.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   29 Jul 2021

@brief  user representation

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from .. import exceptions


class UserError(exceptions.RoboDjudgeError):
    pass


class User():
    tablename = "users"
    rolestable = "roles"
    assignmenttable = "roleassignments"

    def __init__(self, username, roles, pk):
        self.username = username
        self.roles = roles
        self.pk = pk

    def __hash__(self):
        return hash(self.pk)

    def __eq__(self, other):
        return self.pk == other.pk

    def __repr__(self):
        return f"{self.roles} '{self.username}' (pk: {self.pk})"

    def get_praw(self, reddit_instance):
        """
        return the praw Redditor instance
        Keyword Arguments:
        reddit_instance -- a praw reddit instance
        """
        return reddit_instance.redditor(self.username)

    @classmethod
    def insert_manual(cls, connection, username, roles):
        if isinstance(roles, str):
            roles = [roles]
        with connection:
            cur = connection.cursor()
            command = (f"INSERT INTO {cls.tablename}"
                       "(username) VALUES (?)")
            try:
                cur.execute(command, (username,))
            except Exception as err:
                raise type(err)(
                    f"Caught '{err}' while trying to insert {username}")
            user_pk = cur.lastrowid
            user = User(username, list(), user_pk)
            for role in roles:
                cls.add_role(connection, user, role)
                pass
        return user

    @classmethod
    def insert(cls, connection, redditor, roles):
        """
        Keyword Arguments:
        connection -- sql db connection
        redditor   -- a praw Redditor instance
        roles      -- categories of user (e.g., "participant", "djudge"
        """
        # force reddit to update the user and make sure it exitst
        # and that the username is in the correct case
        # (the RoboDjudge is case-sensitive)
        _ = redditor.id
        return cls.insert_manual(connection, redditor.name, roles)

    @classmethod
    def add_role(cls, connection, user, role):
        with connection:
            cur = connection.cursor()
            command = (
                f"SELECT pk FROM {cls.rolestable} WHERE role = ?")
            cur.execute(command, (role,))
            row = cur.fetchone()
            if row is None:
                raise UserError(
                    f"Cannot assign the role '{role}' to user '{user}'. Reason: "
                    "role does not exist in the DB.")
            role_pk = row[0]

            command = (
                f"INSERT INTO {cls.assignmenttable} (user_pk, role_pk) "
                f"VALUES (?, ?);")
            cur.execute(command, (user.pk, role_pk))
            try:
                user.roles.append(role)
            except AttributeError as err:
                raise AttributeError(
                    f"caught '{err}' while adding role '{role}' to user '{user}'")
        return user

    @classmethod
    def exists(cls, connection, redditor):
        cur = connection.cursor()
        command = (f"SELECT count(*) FROM {cls.tablename} "
                   "WHERE lower(username)=?")
        cur.execute(command, (redditor.name.lower(),))
        return cur.fetchone()[0] == 1

    @classmethod
    def select(cls, connection, pk):
        cur = connection.cursor()
        command = (f"SELECT * FROM {cls.tablename} "
                   "WHERE pk=?")
        cur.execute(command, (pk,))
        try:
            pk, username = cur.fetchone()
        except TypeError as err:
            raise TypeError(f"Couldn't fetch user with pk '{pk}'")
        roles = cls.get_roles(connection, pk)
        return cls(username, roles, pk)

    @classmethod
    def get_roles(cls, connection, pk):
        cur = connection.cursor()
        command = (
            f"SELECT {cls.rolestable}.role "
            f"FROM {cls.rolestable}, {cls.assignmenttable} "
            f"WHERE {cls.rolestable}.pk =  {cls.assignmenttable}.role_pk "
            f"AND {cls.assignmenttable}.user_pk = ?")
        cur.execute(command, (pk, ))
        return [row[0] for row in cur.fetchall()]

    @classmethod
    def all(cls, connection):
        cur = connection.cursor()
        command = (f"SELECT pk FROM {cls.tablename} ")
        cur.execute(command)
        return [cls.select(connection, pk[0]) for pk in cur.fetchall()]

    @classmethod
    def fetch(cls, connection, redditor):
        cur = connection.cursor()
        command = (f"SELECT * FROM {cls.tablename} "
                   "WHERE lower(username)=?")
        cur.execute(command, (redditor.name.lower(),))
        try:
            pk, username = cur.fetchone()
        except TypeError as err:
            raise TypeError(
                f"Couldn't fetch user with username '{redditor.name}'")
        roles = cls.get_roles(connection, pk)
        if cls == User:
            return User(username, roles, pk)
        else:
            ret_val = cls(username, pk)
            if ret_val.role != roles:
                raise UserError(
                    f"The user you were trying to fetch is of role "
                    f"'{roles}', but you used the class for  the role "
                    f"'{ret_val.role}")
            return ret_val

    @classmethod
    def create_table(cls, connection):
        with connection:
            cur = connection.cursor()
            command = (f"CREATE TABLE {cls.tablename} "
                       f"(pk INTEGER PRIMARY KEY AUTOINCREMENT, "
                       "  username TEXT NOT NULL UNIQUE);")
            cur.execute(command)
            command = (f"CREATE TABLE {cls.rolestable} "
                       f"(pk INTEGER PRIMARY KEY AUTOINCREMENT, "
                       "  role TEXT UNIQUE NOT NULL);")
            cur.execute(command)
            command = (f"CREATE TABLE {cls.assignmenttable} "
                       f"(user_pk INTEGER NOT NULL, "
                       "  role_pk INTEGER NOT NULL, "
                       f"FOREIGN KEY (user_pk) REFERENCES {cls.tablename}, "
                       f"FOREIGN KEY (role_pk) REFERENCES {cls.rolestable}, "
                       f"UNIQUE (user_pk, role_pk));")
            cur.execute(command)


class UserFactory():
    def __init__(self, role):
        """
        Keyword Arguments:
        tablename -- Where to store these users
        """
        self.role = role

        class Role():
            role = self.role

            @classmethod
            def insert(cls, connection, redditor):
                if not User.exists(connection, redditor):
                    return User.insert(connection, redditor, [role])
                else:
                    user = User.fetch(connection, redditor)
                    return cls.add_role(connection, user)

            @classmethod
            def add_role(cls, connection, user):
                return User.add_role(connection, user, role)

            @classmethod
            def exists(cls, connection, redditor):
                cur = connection.cursor()
                command = (
                    f"SELECT count(*) FROM {User.tablename}, "
                    f"{User.assignmenttable}, {User.rolestable} "
                    f"WHERE {User.rolestable}.role=? "
                    f"AND {User.rolestable}.pk = {User.assignmenttable}.role_pk "
                    f"AND {User.tablename}.pk = {User.assignmenttable}.user_pk "
                    f"AND {User.tablename}.username = ?;")
                cur.execute(command, (role, redditor.name))
                return cur.fetchone()[0] == 1


            @classmethod
            def all(cls, connection):
                cur = connection.cursor()
                command = (
                    f"SELECT {User.tablename}.pk "
                    f"FROM {User.tablename}, {User.assignmenttable}, "
                    f"     {User.rolestable}  "
                    f"WHERE {User.rolestable}.role=? "
                    f"AND "
                    f"  {User.rolestable}.pk = {User.assignmenttable}.role_pk "
                    f"AND "
                    f"  {User.tablename}.pk = {User.assignmenttable}.user_pk;")
                cur.execute(command, (cls.role, ))
                return [cls.select(connection, pk[0]) for pk in cur.fetchall()]

            @classmethod
            def check_role(cls, user):
                if role not in user.roles:
                    raise UserError(
                        f"user with pk = {user.pk} is of roles '{user.roles}' "
                        f"instead of '{role}' ({user})")
                return user

            @classmethod
            def select(cls, connection, pk):
                return cls.check_role(User.select(connection, pk))

            @classmethod
            def fetch(cls, connection, redditor):
                return cls.check_role(User.fetch(connection, redditor))

            @classmethod
            def create_table(cls, connection):
                with connection:
                    cur = connection.cursor()
                    command = f"INSERT INTO {User.rolestable} (role)  VALUES (?)"
                    cur.execute(command, (cls.role, ))

        self.cls = Role


Participant = UserFactory("Participant").cls
Djudge = UserFactory("Djudge").cls
