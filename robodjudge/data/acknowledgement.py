#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   acknowledgement.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   02 Sep 2021

@brief  persistent storage for acknowledgements to sotd posts

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from .. import exceptions
from . import Post

import praw


class AcknowledgementError(exceptions.RoboDjudgeError):
    pass


class Acknowledgement():
    tablename = "acknowledgements"

    def __init__(self, post, pk):
        """
        Keyword Arguments:
        post -- post being acknowledged
        pk   -- primary key
        """
        self.post = post
        self.pk = pk

    @classmethod
    def insert(cls, connection, post):
        if isinstance(post, praw.models.Submission):
            post = Post.fetch(connection, post)
        cursor = connection.cursor()
        command = (
            f"INSERT INTO {cls.tablename} "
            "(post_pk) VALUES (?)")
        cursor.execute(command, (post.pk, ))
        return cls(post, cursor.lastrowid)

    @classmethod
    def exists(cls, connection, post):
        cursor = connection.cursor()
        command = (
            f"SELECT count(*) from {cls.tablename} "
            "WHERE pk = ?;")
        cursor.execute(command, (post.pk, ))
        return cursor.fetchone()[0] == 1

    @classmethod
    def select(cls, connection, pk):
        cursor = connection.cursor()
        command = (
            f"SELECT post_pk from {cls.tablename} "
            "WHERE pk = ?;")
        cursor.execute(command, (pk, ))
        return cls(Post.select(connection, cursor.fetchone()[0]), pk)

    @classmethod
    def fetch(cls, connection, post):
        if isinstance(post, praw.models.Submission):
            post = Post.fetch(connection, post)
        cursor = connection.cursor()
        command = (
            f"SELECT pk from {cls.tablename} "
            "WHERE post_pk = ?;")
        cursor.execute(command, (post.pk, ))
        return cls(Post.select(connection, post.pk), cursor.fetchone()[0])

    @classmethod
    def get_unacknowledged_posts(cls, connection, limit=1):
        cursor = connection.cursor()
        command = (
            f"SELECT {Post.tablename}.pk "
            f"FROM {Post.tablename} "
            f"WHERE 0 = ( "
            f"   SELECT count(*) FROM {cls.tablename} "
            f"   WHERE {cls.tablename}.post_pk = {Post.tablename}.pk) "
            "LIMIT ?;")

        cursor.execute(command, (limit, ))
        return (Post.select(connection, row[0]) for row in cursor.fetchall())

    @classmethod
    def create_table(cls, connection):
        with connection:
            cursor = connection.cursor()
            command = (
                f"CREATE TABLE {cls.tablename} "
                "(pk INTEGER PRIMARY KEY AUTOINCREMENT, "
                "post_pk INTEGER UNIQUE NOT NULL, "
                f"FOREIGN KEY (post_pk) REFERENCES {Post.tablename});")
            cursor.execute(command)
