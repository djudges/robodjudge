#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   djudge_invitation.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>xo

@date   04 Aug 2021

@brief  tracking djudge's invitations

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import secrets
from datetime import datetime

from . import (Post, Djudge, User)
from .. import exceptions


class DjudgeInvitationError(exceptions.RoboDjudgeError):
    pass


class DjudgeInvitation():
    tablename = "djudge_invitations"

    def __init__(self, post, djudge, safety_token, status, timestamp, pk):
        """
        Keyword Arguments:
        post   -- foreign key
        djudge -- foreign key
        status    -- "open", "djudged", "reminded", or "delinquent"
        safety_token -- unique id for this request creates a secret between the
                        djudge and bot (to avoid falsified djudgements by
                        impostors
        timestamp -- time of last status change
        pk        -- primary key
        """
        self.post = post
        self.djudge = djudge
        self.status = status
        self.safety_token = safety_token
        self.last_status_change = timestamp
        self.pk = pk

    @classmethod
    def insert(cls, connection, post, djudge, status="invited"):
        """
        Keyword Arguments:
        connection --
        post       --
        djudge     --
        status     -- (default "invited")
        """
        with connection:
            cursor = connection.cursor()
            command = (
                f"INSERT INTO {cls.tablename} "
                "(post_pk, djudge_pk, safety_token, status, timestamp) "
                "VALUES (?, ?, ?, ?, ?)")
            safety_token = secrets.token_urlsafe(8)
            Djudge.check_role(djudge)
            now = datetime.now()
            cursor.execute(command, (post.pk, djudge.pk, safety_token, status,
                                     now.timestamp()))
            return DjudgeInvitation(post, djudge, safety_token, status, now,
                                    cursor.lastrowid)

    @classmethod
    def select(cls, connection, pk):
        cursor = connection.cursor()
        command = (
            f"SELECT post_pk, djudge_pk, safety_token, status, timestamp from "
            f"{cls.tablename} WHERE pk = ?")
        cursor.execute(command, (pk,))
        post_pk, djudge_pk, safety_token, status, timestamp = cursor.fetchone()
        return DjudgeInvitation(post=Post.select(connection, post_pk),
                                djudge=Djudge.select(connection, djudge_pk),
                                safety_token=safety_token,
                                status=status,
                                timestamp=datetime.fromtimestamp(timestamp),
                                pk=pk)

    @classmethod
    def exists(cls, connection, pk):
        cursor = connection.cursor()
        command = (
            f"SELECT count(*) FROM "
            f"{cls.tablename} WHERE pk = ?")
        cursor.execute(command, (pk,))
        return cursor.fetchone()[0] == 1

    @classmethod
    def fetch_open(cls, connection):
        cursor = connection.cursor()
        command = (
            f"SELECT * FROM {cls.tablename} "
            "WHERE status != 'djudged' "
            "ORDER BY status;" )
        cursor.execute(command)
        return (DjudgeInvitation(Post.select(connection, post_pk),
                                 Djudge.select(connection, djudge_pk),
                                 safety_token, status,
                                 datetime.fromtimestamp(timestamp), pk)
                for (pk, post_pk, djudge_pk, safety_token, status, timestamp)
                in cursor.fetchall())

    @classmethod
    def all(cls, connection):
        cursor = connection.cursor()
        command = (
            f"SELECT * FROM {cls.tablename} "
            "ORDER BY status;" )
        cursor.execute(command)
        return (DjudgeInvitation(Post.select(connection, post_pk),
                                 Djudge.select(connection, djudge_pk),
                                 safety_token, status,
                                 datetime.fromtimestamp(timestamp), pk)
                for (pk, post_pk, djudge_pk, safety_token, status, timestamp)
                in cursor.fetchall())

    @classmethod
    def count_open_invitations(cls, connection):
        cursor = connection.cursor()
        command = (
            f"SELECT COUNT(*) FROM {cls.tablename} "
            "WHERE status != 'djudged' and status != 'delinquent'")
        cursor.execute(command)
        return cursor.fetchone()[0]

    @classmethod
    def count_status(cls, connection, status):
        cursor = connection.cursor()
        command = (
            f"SELECT COUNT(*) FROM {cls.tablename} "
            "WHERE status = ?")
        cursor.execute(command, (status, ))
        return cursor.fetchone()[0]

    def __repr__(self):
        return (f"invitation (id {self.safety_token}) to djudge {self.djudge} for post {self.post}"
                f". Status is {self.status}.")

    @classmethod
    def fetch(cls, connection, safety_token):
        cursor = connection.cursor()
        command = (f"SELECT * FROM {cls.tablename} "
                   "WHERE safety_token = ?")
        cursor.execute(command, (safety_token,))
        row = cursor.fetchone()
        if row is None:
            print('PRINTING ALL INVITES')
            [print(iv) for iv in cls.all(connection)]
            raise DjudgeInvitationError(
                f"There is no invitation with safety token '{safety_token}'")
        (pk, post_pk, djudge_pk, safety_token, status, timestamp) = row
        return DjudgeInvitation(Post.select(connection, post_pk),
                                Djudge.select(connection, djudge_pk),
                                safety_token, status,
                                datetime.fromtimestamp(timestamp), pk)

    def __update(self, connection, status):
        """
        Keyword Arguments:
        status --
        """
        with connection:
            cursor = connection.cursor()
            command = (
                f"UPDATE {self.tablename} "
                "SET status = ?, timestamp = ? "
                "WHERE pk = ?")
            now = datetime.now()
            cursor.execute(command, (status, now.timestamp(), self.pk))
        self.status = status
        self.timestamp = now

    def set_djudged(self, connection):
        """
        sets status to "djudged" in the db
        """
        self.__update(connection, "djudged")

    def set_reminded(self, connection):
        """
        sets status to "reminded" in the db
        """
        self.__update(connection, "reminded")

    def set_delinquent(self, connection):
        """
        sets status to "delinquent" in the db
        """
        self.__update(connection, "delinquent")

    @classmethod
    def invites_for_post(cls, connection, post):
        """
        Keyword Arguments:
        postd --
        """
        cursor = connection.cursor()
        command = (f"SELECT * FROM {cls.tablename} "
                   "WHERE post_pk = ?")
        cursor.execute(command, (post.pk, ))
        return [DjudgeInvitation(post=Post.select(connection, post_pk),
                                 djudge=Djudge.select(connection, djudge_pk),
                                 status=status, pk=pk,
                                 safety_token=safety_token,
                                 timestamp=datetime.fromtimestamp(timestamp))
                for (pk, post_pk, djudge_pk, safety_token, status, timestamp)
                in cursor.fetchall()]

    @classmethod
    def create_table(cls, connection):
        """
        Keyword Arguments:
        connection --
        """
        with connection:
            cursor = connection.cursor()
            command = (f"CREATE TABLE {cls.tablename} "
                       "(pk INTEGER PRIMARY KEY AUTOINCREMENT, "
                       " post_pk INTEGER NOT NULL, "
                       " djudge_pk INTEGER NOT NULL, "
                       " safety_token TEXT UNIQUE NOT NULL, "
                       " status TEXT NOT NULL, "
                       " timestamp INTEGER NOT NULL, "
                       " UNIQUE (djudge_pk, post_pk), "
                       f"FOREIGN KEY (post_pk) REFERENCES {Post.tablename}, "
                       f"FOREIGN KEY (djudge_pk) REFERENCES {User.tablename});")
            cursor.execute(command)
