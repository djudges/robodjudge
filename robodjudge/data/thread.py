#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   thread.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   30 Jul 2021

@brief  representation of a thread

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from .. import exceptions

from datetime import date

class ThreadError(exceptions.RoboDjudgeError):
    pass


class Thread():
    tablename = "threads"

    def __init__(self, title, id, permalink, harvested, created_utc, pk):
        """
        Keyword Arguments:
        title         --
        id            -- reddit id of this submission
        harvested     -- whether this thread has already been handled
        pk            --
        """
        self.title = title
        self.id = id
        self.permalink = permalink
        self.harvested = bool(harvested)
        self.created_utc = created_utc
        self.pk = pk

    def datetime(self):
        return datetime.fromtimestamp(self.created_utc)

    def get_praw(self, reddit_instance):
        """
        return the praw Redditor instance
        Keyword Arguments:
        reddit_instance -- a praw reddit instance
        """
        return reddit_instance.submission(self.id)

    @classmethod
    def insert_manual(cls, connection, title, praw_id, permalink, created_utc):
        with connection:
            cursor = connection.cursor()
            command = (f"INSERT INTO {cls.tablename}"
                       "(title, id, permalink, created_utc) "
                       "VALUES (?, ?, ?, ?)")
            cursor.execute(command, (title, praw_id, permalink, created_utc))
        harvested = False
        return cls(title, praw_id, permalink, harvested, created_utc,
                   cursor.lastrowid)

    @classmethod
    def insert(cls, connection, praw_submission):
        """
        Keyword Arguments:
        connection --
        submission --
        """
        return cls.insert_manual(connection, praw_submission.title,
                                 praw_submission.id, praw_submission.permalink,
                                 praw_submission.created_utc)

    @classmethod
    def select(cls, connection, pk):
        cursor = connection.cursor()
        command = (f"SELECT * FROM {cls.tablename} "
                   "WHERE pk=?")
        cursor.execute(command, (pk, ))
        try:
            (pk, title, praw_id, permalink, harvested,
             created_utc) = cursor.fetchone()
        except TypeError as err:
            raise TypeError(f"Couldn't fetch submission with pk '{pk}'")
        return cls(title, praw_id, permalink, harvested, created_utc, pk)

    @classmethod
    def exists(cls, connection, praw_submission):
        cursor = connection.cursor()
        command = (f"SELECT COUNT(*) FROM {cls.tablename} "
                   "WHERE id=?")

        cursor.execute(command, (praw_submission.id, ))
        return cursor.fetchone()[0] == 1

    def set_harvested(self, connection):
        with connection:
            cursor = connection.cursor()
            command = (
                f"UPDATE {self.tablename} "
                "SET harvested = 1 "
                "WHERE pk = ?")
            cursor.execute(command, (self.pk, ))
        self. harvested = True

    @classmethod
    def all(cls, connection):
        cursor = connection.cursor()
        command = (
            "SELECT title, id, permalink, harvested, created_utc, pk "
            f"FROM {cls.tablename} ")
        cursor.execute(command)
        return (cls(*row) for row in cursor.fetchall())

    @classmethod
    def get_all(cls, connection, harvested):
        cursor = connection.cursor()
        command = (
            "SELECT title, id, permalink, harvested, created_utc, pk "
            f"FROM {cls.tablename} "
            "WHERE harvested = ?")
        cursor.execute(command, (harvested, ))
        return (cls(*row) for row in cursor.fetchall())

    @classmethod
    def fetch(cls, connection, praw_submission):
        cur = connection.cursor()
        command = (f"SELECT * FROM {cls.tablename} "
                   "WHERE id=?")
        cur.execute(command, (praw_submission.id,))
        try:
            (pk, title, praw_id, permalink,
             harvested, created_utc) = cur.fetchone()
        except TypeError as err:
            raise TypeError(
                f"Couldn't fetch thread with id "
                f"'{praw_submission.id}'")
        return cls(title, praw_id, permalink, harvested, created_utc, pk)

    @classmethod
    def create_table(cls, connection):
        with connection:
            cur = connection.cursor()
            command = (f"CREATE TABLE {cls.tablename} "
                       f"(pk INTEGER PRIMARY KEY AUTOINCREMENT, "
                       " title TEXT NOT NULL, "
                       " id TEXT NOT NULL UNIQUE, "
                       " permalink TEXT NOT NULL UNIQUE, "
                       " harvested INTEGER DEFAULT 0,"
                       " created_utc INTEGER NOT NULL);")
            cur.execute(command)
