#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   reminder.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   24 Aug 2021

@brief  persistent storage for reminders

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from .. import exceptions
from . import DjudgeInvitation

from datetime import datetime


class ReminderError(exceptions.RoboDjudgeError):
    pass


class Reminder():
    tablename = "reminders"
    def __init__(self, safety_token, reminder_datetime, pk):
        """
        Keyword Arguments:
        safety_token      -- 
        reminder_datetime -- 
        pk                -- 
        """
        self.safety_token = safety_token
        self.reminder_datetime = reminder_datetime
        self.pk = pk

    def __repr__(self):
        return (f"Reminder({self.reminder_datetime.isoformat()}, "
                "{self.safety_token})")

    @classmethod
    def all(cls, connection):
        cursor = connection.cursor()
        command = (
            f"SELECT * FROM {cls.tablename} ORDER BY reminder_datetime;")
        cursor.execute(command)
        return [cls(safety_token,
                    datetime.fromtimestamp(reminder_datetime),
                    pk)
                for (pk, safety_token, reminder_datetime)
                in cursor.fetchall()]

    def delete(self, connection):
        with connection:
            cursor = connection.cursor()
            command = (
                f"DELETE FROM {self.tablename} "
                "WHERE pk = ?;")
            cursor.execute(command, (self.pk, ))

    @classmethod
    def exists(cls, connection, safety_token):
        cursor = connection.cursor()
        command = (
            f"SELECT count(*) FROM {cls.tablename} "
            "WHERE safety_token = ?;")
        cursor.execute(command, (safety_token, ))
        return cursor.fetchone()[0] == 1

    @classmethod
    def purge(cls, connection, safety_token):
        with connection:
            cursor = connection.cursor()
            command = (
                f"DELETE FROM {cls.tablename} "
                "WHERE safety_token = ?;")
            cursor.execute(command, (safety_token, ))

    @classmethod
    def insert(cls, connection, safety_token, reminder_datetime):
        with connection:
            cursor = connection.cursor()
            command = (
                f"INSERT INTO {cls.tablename} "
                "(safety_token, reminder_datetime) "
                "VALUES (?, ?);")
            cursor.execute(command,
                           (safety_token, reminder_datetime.timestamp()))
            return cls(safety_token, reminder_datetime, cursor.lastrowid)

    @classmethod
    def create_table(cls, connection):
        with connection:
            cursor = connection.cursor()
            command = (
                f"CREATE TABLE {cls.tablename} "
                "( pk INTEGER PRIMARY KEY AUTOINCREMENT, "
                " safety_token TEXT UNIQUE NOT NULL, "
                " reminder_datetime INTEGER NOT NULL, "
                "FOREIGN KEY (safety_token) REFERENCES "
                f"{DjudgeInvitation.tablename});")
            cursor.execute(command)
