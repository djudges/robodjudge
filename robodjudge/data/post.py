#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   post.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   30 Jul 2021

@brief  database representation of sotd posts

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from .. import exceptions
from . import User, Participant
from .thread import Thread

from datetime import datetime


class PostError(exceptions.RoboDjudgeError):
    pass


class Post():
    tablename = "posts"

    def __init__(self, comment_id, text, author, thread, time_of_storage,
                 permalink, created_utc, pk):
        """
        Keyword Arguments:
        comment_id      -- reddit id string
        text            -- this is the body of the post
        author          -- username of author
        thread          -- associated Thread object
        time_of_storage -- datetime object when the post was scraped
        """
        self.comment_id = comment_id
        self.body = text
        self.author = author
        self.thread = thread
        self.time_of_storage = time_of_storage
        self.permalink = permalink
        self.created_utc = created_utc
        self.pk = pk

    def __repr__(self):
        return f"Post by u/'{self.author.username} (pk: {self.pk})"

    def get_praw(self, reddit_instance):
        """
        return the praw Redditor instance
        Keyword Arguments:
        reddit_instance -- a praw reddit instance
        """
        return reddit_instance.comment(self.comment_id)

    @classmethod
    def insert_manual(cls, connection, comment_id, text, author_pk,
                      thread_pk, permalink, created_utc):
        """
        Keyword Arguments:
        connection
        comment_id      --
        text            --
        author_pk       --
        thread_pk       --
        time_of_storage --
        permalink       --
        created_utc     --
        """
        with connection:
            cur = connection.cursor()
            command = (
                f"INSERT INTO {cls.tablename}"
                " (comment_id, body, author_pk, thread_pk,  timestamp, "
                "permalink, created_utc) "
                " VALUES "
                " (?, ?, ?, ?, ?, ?, ?);")
            now = datetime.now()
            cur.execute(command, (comment_id, text,
                                  author_pk,
                                  thread_pk,
                                  datetime.timestamp(now),
                                  permalink,
                                  created_utc))

        return Post(comment_id, text,
                    Participant.select(connection, author_pk),
                    Thread.select(connection, thread_pk), now,
                    permalink, created_utc, cur.lastrowid)

    @classmethod
    def insert(cls, connection, comment):
        """
        Keyword Arguments:
        connection --
        comment    --
        """
        author = (Participant.fetch(connection, comment.author)
                  if Participant.exists(connection, comment.author)
                  else Participant.insert(connection, comment.author))
        thread = (Thread.fetch(connection, comment.submission)
                  if Thread.exists(connection, comment.submission)
                  else Thread.insert(connection, comment.submission))
        return cls.insert_manual(connection, comment.id, comment.body,
                                 author.pk,
                                 thread.pk,
                                 comment.permalink,
                                 comment.created_utc)

    @classmethod
    def select(cls, connection, pk):
        """
        Keyword Arguments:
        connection --
        pk         --
        """
        cursor = connection.cursor()
        command = f"SELECT * FROM {cls.tablename}  WHERE pk=?;"
        cursor.execute(command, (pk,))
        vals = cursor.fetchone()
        if vals is None:
            raise Exception(f"Post with pk {pk} not found")
        (pk, comment_id, text, author_pk, thread_pk, stamp, permalink,
         created_utc) = vals

        return cls(comment_id, text, Participant.select(connection, author_pk),
                   Thread.select(connection, thread_pk),
                   datetime.fromtimestamp(stamp), permalink, created_utc, pk)

    @classmethod
    def all(cls, connection):
        """
        Keyword Arguments:
        connection --
        pk         --
        """
        cursor = connection.cursor()
        command = f"SELECT * FROM {cls.tablename};"
        cursor.execute(command)
        return (cls(comment_id, text,
                    Participant.select(connection, author_pk),
                    Thread.select(connection, thread_pk),
                    datetime.fromtimestamp(stamp), permalink, created_utc, pk)
                for (pk, comment_id, text, author_pk, thread_pk, stamp,
                     permalink, created_utc) in cursor.fetchall())

    @classmethod
    def fetch(cls, connection, praw_comment):
        cursor = connection.cursor()
        command = f"SELECT * FROM {cls.tablename}  WHERE comment_id=?;"
        cursor.execute(command, (praw_comment.id,))
        try:
            (pk, comment_id, text, author_pk,
             thread_pk, stamp, permalink, created_utc) = cursor.fetchone()
        except TypeError as err:
            raise TypeError(
                f"Couldn't fetch submission with id '{praw_comment.id}'")
        return cls(comment_id, text, Participant.select(connection, author_pk),
                   Thread.select(connection, thread_pk),
                   datetime.fromtimestamp(stamp), permalink, created_utc, pk)

    @classmethod
    def exists(cls, connection, comment):
        cursor = connection.cursor()
        command = f"SELECT pk FROM {cls.tablename}  WHERE comment_id=?;"
        cursor.execute(command, (comment.id,))
        return len(cursor.fetchall()) == 1

    @classmethod
    def create_table(cls, connection):
        with connection:
            cursor = connection.cursor()
            command = (f"CREATE TABLE {cls.tablename} "
                       "(pk INTEGER PRIMARY KEY AUTOINCREMENT, "
                       "comment_id TEXT NOT NULL UNIQUE, "
                       "body TEXT NOT NULL, "
                       "author_pk INTEGER NOT NULL, "
                       "thread_pk INTEGER NOT NULL, "
                       "timestamp INTEGER NOT NULL, "
                       "permalink TEXT NOT NULL, "
                       "created_utc INTEGER NOT NULL, "
                       f"FOREIGN KEY (author_pk) REFERENCES "
                       f"{User.tablename}, "
                       f"FOREIGN KEY (thread_pk) REFERENCES "
                       f"{Thread.tablename});")
            cursor.execute(command)
