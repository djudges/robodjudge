#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   fetchers.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   31 Jul 2021

@brief  functions to fetch threads and sotds

Copyright © 2021 Djundjila

Robodjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Robodjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import re
from datetime import (datetime, timedelta)

re_SOTD = re.compile(
    "(Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday) SOTD Thread.*")

re_Austere_August = re.compile(
    "(Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday) Austere August SOTD Thread.*")


def fetch_subreddit(reddit_instance, name):
    return reddit_instance.subreddit(name)

def fetch_threads(subreddit, re_titles, since, limit=300):
    for submission in subreddit.new(limit=limit):
        if (datetime.fromtimestamp(submission.created_utc) >=
            datetime.combine(since, datetime.min.time())):
            for re_title in re_titles:
                if re_title.match(submission.title):
                    yield submission

def fetch_toplevel_comments(thread):
    for comment in thread.comments:
        yield comment

def filter_comments(comments, hashtag):
    for comment in comments:
        if hashtag.lower() in comment.body.lower():
            yield comment

def stream_comments(subreddit):
    return subreddit.stream.comments()

def stream_comments_from_newest_submission_of_matching_set(subreddit,
        stream, submission_title_patterns, since, limit=300):

    refresh_period = timedelta(seconds = 600)
    last_estimate_time=datetime.now() - refresh_period - refresh_period
    last_estimate = last_estimate_time
    def estimate_newest_submission_time():
        nonlocal last_estimate_time
        nonlocal last_estimate
        now = datetime.now()
        elapsed = now - last_estimate_time
        if elapsed > refresh_period:
            threads = list()
            for thread in fetch_threads(subreddit, submission_title_patterns, since, limit):
                threads.append(thread)
            if len(threads) == 0:
                print(f"WARNING: Found zero threads matching the patterns {patterns}")
                return datetime.now().timestamp()
            last_estimate = (
                sorted(threads, key=lambda thread:thread.created_utc)[-1].created_utc)
            last_estimate_time = now
        return last_estimate

    def match(submission):
        for pattern in submission_title_patterns:
            if pattern.match(submission.title):
                return True
        return False

    for comment in stream:
        if (comment.submission.created_utc >= estimate_newest_submission_time()
            and match(comment.submission)):
            yield comment

def stream_comments_filter_toplevel(stream):
    for comment in stream:
        if comment.parent_id == comment.link_id:
            yield comment

def retrieve_existing_and_stream_future_submissions(subreddit, patterns,
                                                    since, max_nb_threads,
                                                    limit=300):
    nb_to_do = max_nb_threads
    threads = list()
    for thread in fetch_threads(subreddit, patterns, since, limit):
        threads.append(thread)
    threads.sort(key=lambda thread: thread.created_utc)
    for thread in threads:
        nb_to_do -= 1
        yield thread
        if nb_to_do < 1:
            break

    for thread in subreddit.stream.submissions(skip_existing=True):
        for pattern in patterns:
            if pattern.match(thread.title):
                nb_to_do -= 1
                yield thread
                if nb_to_do < 1:
                    break

def retrieve_existing_and_stream_future_comments(
        subreddit, patterns, since, limit=300):
    threads = list()
    for thread in fetch_threads(subreddit, patterns, since, limit):
        threads.append(thread)
    threads.sort(key=lambda thread: thread.created_utc)
    for thread in threads:
        for comment in fetch_toplevel_comments(thread):
            yield comment

    stream = stream_comments(subreddit)
    stream = stream_comments_from_newest_submission_of_matching_set(subreddit,
        stream, patterns, since, limit)
    stream = stream_comments_filter_toplevel(stream)
    for comment in stream:
        yield comment
