#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   __init__.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>xo

@date   01 Aug 2021

@brief  module for robotic djudging

Copyright © 2021 Djundjila

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__all__ = ["data", "logistics",
           "message_with_rate_limit", "truncate_str",
           "truncate_subject", "truncate_body",
           "fetch_toplevel_comments"]

from .fetchers import fetch_toplevel_comments
from .helpers import (truncate_str, truncate_subject, truncate_body,
                      message_with_rate_limit, reply_with_rate_limit,
                      list_table)
from . import (data, logistics, exceptions)
