#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   setup.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   09 Aug 2021

@brief  RoboDjudge is a library for building reddit bots for judging contests

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from setuptools import setup, find_packages

setup(name="robodjudge",
      packages=find_packages(),
      test_suite="tests")
