#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   warner.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   31 Jul 2021

@brief  Checks Austere August posts for mentions

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from argparse import ArgumentParser
from datetime import datetime
import os
import re
import time
import pickle
import sqlite3

import sys
sys.path.append('..')

import praw
import robodjudge
from robodjudge.data import Thread, Post, User
from robodjudge import (
    re_SOTD, re_Austere_August, retrieve_existing_and_stream_future_comments,
    fetch_subreddit, message_with_rate_limit)

def parse_args():
    parser = ArgumentParser(description="warning about MMOC without hashtag")
    parser.add_argument("--db", type=str, default="warner_data.db",
                        help="path to database file for persistent storage")
    parser.add_argument("--start", type=str, default=datetime.now().isoformat(),
                        help=("starting time from when to consider "
                              "threads. eg '2021-07-31'"))
    parser.add_argument("usernames", type=str, nargs='+',
                        help="users to warn")
    parser.add_argument("mummy", type=str,
                        help="user to cry to when bot crashes")
    parser.add_argument("bot_conf", type=str,
                        help="name fo the config in praw.ini to use")
    args = parser.parse_args()
    args.start = datetime.fromisoformat(args.start)
    return args

def get_connection(args):
    connection = sqlite3.connect(args.db)
    try:
        User.create_table(connection)
        Post.create_table(connection)
        Thread.create_table(connection)
    except Exception:
        pass

    connection.cursor().execute(f"DELETE FROM {User.tablename}")
    connection.commit()
    return connection

def fill_users(args, reddit_instance, connection):
    users = list()
    for username in args.usernames:
        users.append(User.insert(
            connection,
            reddit_instance.redditor(username)).get_praw(reddit_instance))
        _ = users[-1].id # forcing a check of the usernames

    return users

def main():
    args = parse_args()
    reddit_instance = praw.Reddit(
        args.bot_conf,
        user_agent="warner")

    connection = get_connection(args)
    users = fill_users(args, reddit_instance, connection)

    mummy = reddit_instance.redditor(args.mummy)
    mummy_id = mummy.id #forcing the emergency contact to be valid
    sub = fetch_subreddit(reddit_instance, "Wetshaving")

    def truncate_str(text, max_len):
        return text if len(text) <= max_len else text[:max_len-3] + "..."

    def truncate_subject(text):
        max_subject_length = 100
        return truncate_str(text, max_subject_length)

    def truncate_body(text):
        max_body_length = 10000
        return truncate_str(text, max_body_length)

    stream = retrieve_existing_and_stream_future_comments(
        sub, (re_Austere_August,), args.start)
    try:
        for comment in (comment for comment in stream
                        if not Post.exists(connection, comment)):
            body = comment.body.lower()
            if ("mmoc" in body) and ("gemsofwisdom" not in body):
                subject = truncate_subject(
                    f"u/{comment.author.name} may have forgotten the "
                    "GEMSsOfWisdom")
                body = truncate_body(
                    f"{comment.author.name} [mentioned mmoc]"
                    f"({comment.permalink}?context=3) in "
                    f"{comment.submission.title},"
                    f"but hasn't used the GEMsOfWisdom tag. Please check"
                    f"\n\n{comment.body}")
                print(
                    f"\nMessaging about this comment by "
                    f"{comment.author.name}:"
                    f"\n{comment.body}\n")

                for user in users:
                    print(
                        f"Messaging {user.name}")
                    message_with_rate_limit(user, subject, body)
                if not User.exists(connection, comment.author):
                    User.insert(connection, comment.author)
                if not Thread.exists(connection, comment.submission):
                    Thread.insert(connection, comment.submission)
                Post.insert(connection, comment)
                connection.commit()

    except Exception as err:
        message_with_rate_limit(mummy, "Warner crashed", f"{err}")


if __name__ == "__main__":
    main()
