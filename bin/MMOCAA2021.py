#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   MMOCAA2021.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   23 Aug 2021

@brief  djudge for helping with the The first **ever** Austere August Side Side
        Challenge: Introducing the First Annual GEMs of Wisdom: Finding
        Serenity in Austerity Side Side Challenge

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from argparse import ArgumentParser
import sqlite3
import praw
from collections import defaultdict
import numpy as np

from datetime import time, date, datetime, timedelta
import pytz
from robodjudge.logistics import DjudgeClerk
from robodjudge import data, logistics
import secrets


def parse_args():
    parser = ArgumentParser(description="GEMsOfWisdom djudge")
    parser.add_argument("--db", type=str, default="GEMsOfWisdom_data.db",
                        help="path to database file for persistent storage")
    parser.add_argument("djudgenames", type=str, nargs='+',
                        help="djudges to warn")
    parser.add_argument("mummy", type=str,
                        help="user to cry to when bot crashes")
    parser.add_argument("bot_conf", type=str,
                        help="name for the config in praw.ini to use")
    args = parser.parse_args()
    return args


def get_djudgement_class():
    Djudgement = data.DjudgementFactory(
        {"dq": data.BooleanGradeFactory(default_=False).cls,
         "entertainment": data.NumericGradeFactory(0, 5.).cls,
         "insight": data.NumericGradeFactory(0, 5.).cls,
         "comment": data.TextGradeFactory(default_="").cls
         }).cls
    return Djudgement


def get_connection(args):
    connection = sqlite3.connect(args.db)
    for structure in (data.User,
                      data.Djudge,
                      data.Participant,
                      data.Thread,
                      data.Post,
                      data.DjudgeInvitation,
                      data.Reminder,
                      data.Message,
                      get_djudgement_class()):
        try:
            structure.create_table(connection)
        except sqlite3.OperationalError as err:
            if f"{err}" != f"table {structure.tablename} already exists":
                raise err
        except sqlite3.IntegrityError as err:
            if f"{err}" != "UNIQUE constraint failed: roles.role":
                raise err
    return connection


def fill_djudges(args, reddit_instance, connection):
    users = list()
    for djudgename in args.djudgenames:
        djudge = reddit_instance.redditor(djudgename)
        if not data.Djudge.exists(
                connection, djudge):
            data.Djudge.insert(
                connection,
                djudge)


def generate_AA_thread_dict(year=2021, deadline_time=time(23, 59), days=0,
                            timezone=pytz.timezone("US/Pacific")):
    """
    year = calendar year for which to compute the dict
    deadline_time = time at which the thread can be harvested (pencils down!)
    days = how many days later the harvest happens (usually 0)
    timezone = timezone in which the deadline is specified
    """
    def deadline_and_title():
        for day in range(1, 32):
            deadline = timezone.localize(
                datetime.combine(date(year, 8, day),
                                 deadline_time))
            title = deadline.strftime(
                "%A Austere August SOTD Thread - %b %d, %Y")
            yield title, deadline
    return {title: deadline for (title, deadline) in deadline_and_title()}


# todo: turn this off
logistics.generate_AA_thread_dict = generate_AA_thread_dict


def list_table(d, n=("GEMtleperson", "score")):
    nb_items = len(n)
    c = list()
    for item_id in range(nb_items):
        c.append(max(max(
            (len(f"{l[item_id]}") for l in d)), len(n[item_id])))

    retval = list()
    retval.append("| " + " | ".join(
        (f"{ni:<{ci}}" for (ni, ci) in zip(n, c))) + " |")
    retval.append("|:" + c[0]*"-" + "-|-" + ":|-".join(
        (ci * "-" for ci in c[1:])) + ":|")
    for item in d:
        retval.append("| " + " | ".join(
            (f"{ni:<{ci}}" for (ni, ci) in zip(item, c))) + " |")
    return "\n".join(retval)


def compute_leader_board(connection):
    participants = data.Participant.all(connection)
    nb_possible_posts = len(generate_AA_thread_dict())
    grade_dict = dict()
    for participant in participants:
        scores = get_djudgement_class().get_for_participant(connection,
                                                            participant)
        # collecting the scores per post:
        posts_dict = defaultdict(list)
        for score in scores:
            posts_dict[score.post.pk].append(score)
        scores = list()
        for key, values in posts_dict.items():
            vals = list()
            for score in values:
                if score.grades["dq"].value is False:
                    vals.append(.5*(score.grades["entertainment"].value +
                                   score.grades["insight"].value))
                else:
                    vals.append(0.)
            scores.append(np.array(vals).mean())
        try:
            total_score = (np.array(scores).sum()/nb_possible_posts if scores
                           else 0.)
        except TypeError as err:
            raise TypeError(
                f"Caught '{err}' while trying to take the mean of scores '{scores}'. Type of scores[0] is {type(scores[0])}")
        grade_dict[participant.username] = total_score
    leader_bord = sorted(grade_dict.items(), key=lambda x: x[1], reverse=True)
    leader_bord = [(f"u/{name}", score) for (name, score) in leader_bord]
    table = list_table(leader_bord)
    return table


def post_finder_factory(subreddit, connection):
    threads = logistics.generate_AA_thread_dict()
    since = datetime(2021, 7, 31).timestamp()
    counter = defaultdict(int)
    tz = pytz.timezone("us/pacific")

    def posts_filter(stream):
        for post in stream:
            if ("gemsofwisdom" in post.body.lower() and
                    post.created_utc < threads[
                        post.submission.title].timestamp()):
                date_str = datetime.fromtimestamp(
                    post.created_utc, tz).strftime('%c')
                print(
                    f"On {date_str} (PST) by u/{post.author.name}")
                counter[post.author.name] += 1
                yield post
        print(counter)

    fetcher = logistics.PostFetcherDict(
        threads, subreddit, connection, since, posts_filter)
    return fetcher


def main():
    args = parse_args()
    connection = get_connection(args)
    reddit_instance = praw.Reddit(
        args.bot_conf,
        user_agent="RoboDjudge MMOCAA")
    sub = reddit_instance.subreddit("Wetshaving")

    fill_djudges(args, reddit_instance, connection)

    mum = reddit_instance.redditor(args.mummy)

    post_fetcher = post_finder_factory(sub, connection)

    participant_counter = defaultdict(lambda: defaultdict(int))

    def handle_djudge_assignment(self, post, djudges):
        djudgements = {djudge: participant_counter[post.author][djudge]
                       for djudge in djudges if djudge != post.author}
        djudge_lists = defaultdict(list)
        [djudge_lists[count].append(djudge) for (djudge, count)
         in djudgements.items()]
        eligible_djudges = djudge_lists[min(djudge_lists.keys())]
        djudge = secrets.choice(eligible_djudges)
        participant_counter[post.author][djudge] += 1
        self.invite_djudge(post, djudge)

    clerk = DjudgeClerk(reddit_instance, connection, mum,
                        get_djudgement_class(), post_fetcher,
                        ("The MMOC Side Contest and the GEMs of Wisdom: "
                         "Finding Serenity in Austerity Side Side Challenge"),
                        reminder_delay=timedelta(days=1, hours=18),
                        wait_for_stop=True,
                        get_leaderboard=compute_leader_board,
                        handle_djudge_assignment=handle_djudge_assignment)

    clerk.run()


if __name__ == "__main__":
    main()
