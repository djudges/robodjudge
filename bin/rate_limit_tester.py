#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   rate_limit_tester.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>xo

@date   01 Aug 2021

@brief  test the rate limit message functions

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from argparse import ArgumentParser
import praw
import sys
sys.path.append('..')

from robodjudge import message_with_rate_limit

def parse_args():
    parser = ArgumentParser(description="warning about MMOC without hashtag")
    parser.add_argument("dest", type=str,
                        help="user to send messages to")
    parser.add_argument("n", type = int, help="number of messages to send")
    parser.add_argument("bot_conf", type=str,
                        help="name fo the config in praw.ini to use")
    args = parser.parse_args()
    return args

def main():
    args = parse_args()

    reddit_instance = praw.Reddit(
        args.bot_conf,
        user_agent="warner")

    destination = reddit_instance.redditor(args.dest)

    for i in range(args.n):
        message_with_rate_limit(destination, f"test nb {i}", "body")

if __name__ == "__main__":
    main()
