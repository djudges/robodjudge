#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   MMOCAA2021.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   23 Aug 2021

@brief  djudge for helping with the The first **ever** Austere August Side Side
        Challenge: Introducing the First Annual GEMs of Wisdom: Finding
        Serenity in Austerity Side Side Challenge

Copyright © 2021 Djundjila

RoboDjudge is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RoboDjudge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from argparse import ArgumentParser
import sqlite3
import praw
from datetime import datetime
from collections import defaultdict
import numpy as np
import sys
sys.path.append('..')

from datetime import time, date, datetime, timedelta
import pytz
from robodjudge.logistics import DjudgeClerk
from robodjudge import data, logistics

from unittest.mock import MagicMock

from MMOCAA2021 import (get_djudgement_class,
                        get_connection,
                        compute_leader_board)


def parse_args():
    parser = ArgumentParser(description="GEMsOfWisdom djudgement tetser")
    parser.add_argument("db", type=str,
                        help="path to database file for persistent storage")
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    connection = get_connection(args)
    reddit_instance = MagicMock()

    clerk = DjudgeClerk(reddit_instance, connection, MagicMock(),
                        get_djudgement_class(), MagicMock(),
                        ("The MMOC Side Contest and the GEMs of Wisdom: "
                         "Finding Serenity in Austerity Side Side Challenge"),
                        wait_for_stop=True,
                        get_leaderboard=compute_leader_board)

    print("hello, world")
    print(clerk.compile_participation_summary())
    clerk.reddit = praw.Reddit("R41anon", user_agent="manual")
    clerk.add_djudge("eldrormr")


if __name__ == "__main__":
    main()
