#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   inbox_cleaner.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   30 Aug 2021

@brief  Clean out all messages from djudged invites

Copyright © 2021 Djundjila

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import praw
import re
from argparse import ArgumentParser


def parse_args():
    parser = ArgumentParser(
        usage="clean out djudgement invitations from robodjudge and r41anon")
    parser.add_argument("praw_config", type=str, help="praw.ini config touse")
    args = parser.parse_args()

    return args


def find_deletables(inbox):
    re_djudgement = re.compile(
            ".*Inviting Djudge u/([A-Za-z_]+) to djudge "
            "post id (.*)$")

    djudgements = set()
    alerts = set()
    warnings = set()
    for message in inbox.messages(limit=None):
        if not message.new:
            if re_djudgement.match(message.subject):
                djudgements.add(message)
            elif "Notification of received post" == message.subject:
                djudgements.add(message)
            elif ("posted for the MMOC side (side) challenge"
                  in message.subject):
                alerts.add(message)
            elif "may have forgotten the GEMSsOfWisdom" in message.subject:
                warnings.add(message)

    return djudgements, alerts, warnings


def main():
    args = parse_args()
    reddit = praw.Reddit(args.praw_config, user_agent="message_cleaner")

    inbox = reddit.inbox

    djudgements, alerts, warnings = find_deletables(inbox)
    print(f"I found {len(djudgements)} deleteable djudgements, "
          f"{len(warnings)} warnings, and {len(alerts)} alerts.")

    for msg in djudgements | alerts | warnings:
        msg.delete()


if __name__ == "__main__":
    main()
